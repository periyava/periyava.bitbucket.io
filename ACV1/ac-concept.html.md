Title:Concept Of Maya


### Concept of Maya

In the Mangala Sloka(invocation) to this Beeja Ganita(algebra),
Bhaskaracharya says that supreme which is Infinite, does not suffer
diminution when creating the world out of Himself, or gain addition when
the created word attains Laya(merger) in Him. For, if the addition of
even a fraction can make a difference to the infinite, then it could not
have been infinite before such addition. Similarly infinite cannot
become less than the infinite when any thing is taken away from it. The
Infinite is poorna, full and limitless Supreme. The Prapancha (Universe)
which is infinitely varied, is also limitless Supreme, the limitless
Supreme will remain intact. Therefore, if this Poornam ( the infinitely
varied form of the objective Prapancha) is taken away from that Poornam
(the subject which is Infinite), that Poornam, the subject Infinite,
alone will remain.

This may be illustrated mathematically as follows : if 2 is divided by
2, the quotient is 1. With 2 as the dividend, if the divisor is
progressively reduced as 1, 1/2, or 1/4 etc., the quotient will
respectively be 2, or 4, or 8,etc., Thus the divisor becomes less and
less, the quotient will become more and more. When the divisor is the
least, that is infinitesimal, approximating to Zero, the quotient will
be infinity. This is known as the Khaharam - Kha standing for Aakas,
signifying poojyam (zero), haaram, meaning taking away or dividing.

How do we verify the correctness of an arithmetical question in
division? We multiply the quotient with the divisor and check whether
the resulting is equivalent to the dividend given in the question. In
this Khaharam, or division. In this Khaharam, or division of any number
by zero, the number that is divided stands for the Prapancha 9the
pluralistic universe of infinite variety), the divisor, zero or Poojyam,
which in mathematical language is an indefinable factor, approximating
to nothingness, stands for Maya, and the quotient is the Infinite, that
is Brahman. For the purpose of creating the Prapancha, which is
dividend, Brahman , which is the quotient , multiplies itself by Maya,
which is divisor. Even as I divided by Zero, or 2 divided by Zero, or 3
divided by Zero, will give the same quotient, when the Infinite is
multiplied by Zer, it is undeterminate, and therefore, it can take the
values 1,2,3 etc., which are Bheda sankhyas, or numbers connoting
differences, standing for the plurality of the world. The Upanishad says
that the One Absolute determined to become many, and for that purpose.
It associated itself with Maya, and become Many. When this Absolute
Infinite multiplied Itself in association with Maya, which is tantamount
to zero, it appears as 1,2,3,4 etc., the several objects of this
Prapancha. But when any number is multiplied by Maya. The dividend,
which is the plurality of the prapancha is the Infinite variety. The
quotient, which is Brahman, is Real Akhanda and Ananta. In the Saanti
Mantra, Poornam adah is the quotient , Absolute Infinity, and Poornam
idam is the dividend, pluralistic Infinity. Advaita anantam multiplied
by Poojyam is Dvaita anantam. if the latter is divested of is Maya -- by
a process of Khaharam-dividing by Poojyam which is Maya -- we get the
Advaita anantam. Maya multiplies the formless Infinite which is One only
without a second , into an infinity of finite forms. Th One alone , that
is real, has value; the Many, which are products of Maya, are like Maya,
without ultimate value. So Brahman is not affected either by diminution
from It (creation or Srsti) or by the addition to It(merger or Laya) of
Prapancha, which has no ultimate value.

The Divine Mother is the Creative Principle of the universe, the Maya
Sakti aspect of Brahman, which makes the Infinite One appear as the
Infinite Many. She presents the formless Supreme in finite forms. It is
only by her grace that one can transcend the Maya and obtain the
advaitic realisation of the One without a second.

December 26 1956. 

