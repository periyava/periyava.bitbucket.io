Title:Divine Mother

**DIVINE MOTHER AS KANAKA PARAMESWARI**

(“The message given by His Holiness when he visited the Kothwal Bazar in
Chennai and the Sri Kanyaka Parameswari temple inside the market”).

We wash our bodies and clothes daily in order to get rid of the accreted
dirt and keep them clean. Impurity gets attached to our mind also,
during every waking moment of our life, as a result of bad thoughts,
wrong desires and passions like anger. It behoves us to cleanse our
minds also every day, so that impurity may not go on accumulating and
cause us sorrow, sufferings and difficulties. The only water that can
wash off mental impurity is the water of dhyana or meditation. We
should, therefore, concentrate our thoughts on God at least for a few
moments every day and invoke His grace to cleanse our hearts. However,
bad a man may be, all evil thoughts within him recede to the background
in the presence of his mother. Similarly in the presence of the Divine
Mother, all of us can get rid of our mental impurity. The Divine Mother,
in the form of Sri Kanyaka Parameswari, has been installed in this
temple. The genius of our ancients is responsible for conceiving the
Mother of all creation as a virgin (kanni). It is the duty of all to
visit a temple everyday, meditate on the form of god installed in that
temple, and pray for the washing off of all the impurities of mind. If
we do so, all our troubles will melt away like dew before the sun; we
shall be successful and happy, and our families will also prosper.

The manifestation of the Divine Mother as Sri Kanyaka Parameswari is
sacred to the Arya Vysya community. She is the same Mother, who, in Her
manifestation as Kamakshi, has been installed at Kancheepuram. Sri
Sankara Bhagavatpada has installed a Sri Chakra also inside that temple,
and established the Kamakoti Peetam. The object of establishing various
Peetas by Sri Bhagavatpada was to remind the succeeding generations of
the sacred injunctions contained in the Sastras, so that people may not
get deflected from the right path.

With the passage of time, many changes have crept into our society. Food
prohibited by the Sastras is being eaten and there is generally a
deterioration in the moral standards. The classification of society into
castes and communites was not classification of society or to accentuate
differences. It was intended to form convenient groups, whose welfare
could be attended to by panchayats of elders, without impairing the
solidarity as a whole. Such grouping was based on duties, and the elders
of each group saw to it that the group functioned properly and that
people belonging to the group or community did not deviate from the
right path. When such panchayats or nattanmai functioned, disputes were
settled in the presence of God and wrongdoers were asked to make some
offering to Him. In that way, the moral standards of the people were
being maintained at a high level. The state had to step in only to
protect the people from external aggression. Society functioned as
smoothly as a well-organised factory, each man doing his allotted duty
and all contributing to the common welfare.

As the occupant of the Kamakoti Peetam, I feel it my duty to remind you
of our glorious traditions and of the correct conduct of life as laid
down in the Sastras. The strength of the Peetam is the affection that
voluntarily flows from the hearts of the devotees. It is on this
foundation and with this capital of affection that I can function. What
attracts a foreigner to India is not the wealth of this country, but the
spiritual message she has given and is giving. That message will have
enhanced value when the people of this country adhere to the way of life
laid down in the Sastras – the Sastras that proclaimed the spiritual
message. I hope you will remember this truth and so conduct yourself
with devotion, as to earn the grace of the Divine Mother. May the Divine
Mother bless you with hapiness and prosperity.

April 6, 1958


