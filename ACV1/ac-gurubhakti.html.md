Title:Guru Bhakti

 
 

### Guru bhakti

Who is God and what is His definition? In almost every religion, God is
referred to as the Creator, the karta, responsible for the creation and
sustenance of the Universe. Since every effect must have a cause, namely
God, for this Universe. This is brought out inthe Brahma- Sutra by the
expression karta Saatraartha tvat. Another defition of God is that He is
the dispenser of the fruits of our actions, be they good or evil---
Karmaphala-daata. The question may be asked why we should have be Bhakti
for the God who is the creator and dispenser. These are His self
-chioasen funtions and he does them. Why should we have devotion to one
who created, not at our request, and who dispenses, not according to our
choice?

This question does not arise in the arise in the case of schools of
thought which deny a God altogether. Among the Vaidika systems, the
Saankhya denies a creator -GOd and the Poorva Meemamsa has no use for
Him. It is a non-intelligent principle that is responsible for the world
according to the former, and the dispensation of fruits of actions is
due , according to the latter, to Adrshta, and a God is unnecessary for
the purpos. Sri Sankara Bhagavatpada combated both these positions and
established that a Jada vastu cannot come from another JadaVastu, an
intelligent God alone must be the cause of the Universe. He also showed
that a Chaitanya (power) is necessary to dispense fruits of Karma
according to the merit. In fact, Sri Sankara directed his criticism
mainly against the views of the Saankhyaas and Meemamsakaas respectively
and onlty incidentally against teh Buddhists, though he is depicted as
having Banished Buddhism from the country by the froce of dialectics. As
a matter of fact, it was left to two nea-contemporaries of the Sri
Sankara, Kumarila Bhatta, the Meemamsaka, and Udayanacharya, the
Taartika, to undermine the foundations of Buddhism. Kumarila disproved
the no-Karma plank, and Udayana the no-Isvara plank of Buddhism.

Having established that there is a Creator,who is Srshtikarta and the
Phala-daata,the question remains why should we show Bhakti to Him? The
Yoga sutras of Patanjali provide the answer. After defining Yoga as a
the control of mind's activity, the question of the way to control that
activity comes up for considertation and it is answered that this can be
brought about by worship of God, who free from any imperfection or
blemish ,who remains unmoved and unmovable,who is the Sttaanu(stable
one),amidst the imperfect and instable things of the world.Being the
all-knowing Intelligence,God is not affected by anything which could
distract the mind and prevent its control.It is such an ideal that we
should have before us, to train ourselves in mind-control,so that the
mind may be almost absolutely study like a flame in a place where there
is no brezee.Since concentrated meditation on a thing transforms one
into the likeness of the thing meditated upon,meditating on God,who
being Omniscient is still unmoved and unaffected by want or desire,
makes on like God Himself.As one holds fast to steady pillar to prevent
from being tossed about,so too should one bind oneself through Bhakti to
God,to steady one's mind.

The purpose of prayer is not petition for benifits.Such petitioning
implies either that God does not know what we want,which will militate
against His Omniscience,or that He waits to be asked and delights in
praise,which will degrade Him to the leve of ordinary man.Why then do we
pray? Though Omniscient God is immanent in every creature and knows what
is in the heart of every person, yet, if what we wish to say in prayer
remains unsaid, it afflicts our heart and so prayer heals that
afflicition. By prayer ,we do not seek to change what God ordains; in
fact,we cannot do so. We go to Him to remove our impurities. As
Tiruvalluvar said, we attach ourselves to Him who has no attachments to
rid ourselves of our attachments. A devout consciousness that God exists
will itself do the miracle of alchemising us into purity of nature. We
obtain a spiritual charge into our frame by being in His presence.

Guru is Isvara in human form, but who is, however, freefrom the triple
functions of creration, preservation and destruction, which pertain only
to Isvara. If we have absolute faith in him, the Guru will dower us with
all for which we go to God. In fact, God is needed only when we cannot
find a guru. Guru-Bhakti is even higher and more efficacious than
Daiva-bhakti. Sri Vedanta Desika has declared that he does not consider
God higher than Guru.According to a verse, when God is angry, the guru
protects you; when the guru himself is angry, there is no protector in
the world. If we surrender ourselves absolutely without any reservation
to the guru,he will save us from all sorrow and show us the way to
salvation. It is due to lack of guru-bhakti, that Isvara-bhakti itself
is waning in the hearts of men.

October 20, 1957 

