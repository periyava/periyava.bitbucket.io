Title:Way to Eternal Bliss

### Way to Eternal Bliss


It is only a person who has developed a proper mental outlook that can
face adversities without being unduly perturbed. Such a person will have
the equanimity of mind to comfort those who go to him in any calamity.
On the other hand, there are some persons who are upset even by the
smallest difficulty, because they have not developed the necessary
mental equipoise. Pleasure and pain are inevitable concomitants of life,
and pain has to be endured to face good fortune without undue elation
and misfortune without undue depression is acquired by a proper mental
approach to things of this world. The source of all happiness is within
ourselves. So long as we grope for this source outside ourselves, we
shall not fine peace or develop the necessary mental equilibrium. That
is the significance of the advice of Lord Krishna to Arjuna, when he
said that *Isvara* lives inside the hearts of all creation. Sri Krishna
says further that by His divine power of *maaya*, *Isvara* makes the
universe function in a predetermined pattern, as if motivated or
impelled by a powerful machine. When we do our duty correctly, and
surrender to Him completely and unreservedly – a surrender which
embraces all our mental and physical faculties (*Sarva bhaavena*) – we
become calm mentally and see things in their proper perspective.

 

The peace or *saanti* we aim at is not the outcome of fear, but the
natural corollary to fearlessness. That peace cannot come so long as we
think that God is somewhere beyond our reach. The seeker of saanti must
keep his heart clean and clear (*prasanna*). Then only can we realise
God within us, or, in other words, we can secure  the *prasaada* of
*Isvara.* The two expressions, *prasanna* and *prasaada* are synonymous
*prasaadostu prasannata, says Amara Kosa.* When our hearts are cleansed
of all impurities and we are ready to leave the fruits of our action, be
it punishment or reward, to God. He indwells within us and blesses us
with that supreme saanti (*paraam saanti*). It is only such a bliss that
is eternal (saasvatam); all the other kinds of bliss are transient. This
is the significance of the following verses of the Gita

 

 

 

> Isvarah sarva bhootanaam hrid-dese Arjuna tishthati
> Braamayam sarva bhootaani yantraaroodhaani maayayaa
> **Tameva saranam gaccha sarva-bhaavena Bhaarata**
> Tat prasaadaat paraam saantim sthaanam praapsyasi Saasvatam
