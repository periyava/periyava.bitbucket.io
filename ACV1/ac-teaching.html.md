Title:The Teaching Of Vedanta
 
 

### THE TEACHING OF VEDANTA

In some context or other, we constantly come across or hear the word
"Vedanta". When any person's conversation becomes a little above the
average standard or has the tinge of a sermon, we say " You are talking
Vedanta". In the Gita, the Lord says that He is the origin of Vedanta.
Literally, Vedanta means the end or the concluding portion of the Veda.
In any well-written essay, the writer will indicate the subject matter
at the beginning and record his conclusions at the end. Therefore, any
intelligent person, by reading the Upakrama (beginning and conclusion)
of a thesis, will get any idea of what is about. Similarly, if we take
any section of the Veda and read its beginning and its end, we will be
able to grasp what that section deals with.

The constitution of any country and its laws are limited by time and
place - kaala and desa. But the vedas are the eternal laws or Sanatana
Dharma. That is why when a person asserts a position taken up by him,
though a different view is possible, we say, "Are your words the words
of the Veda?" Isvara is Veda Swaroopi or embodiment of the vedas, and
one of the Veda Mantra says that Maheswara dwells at the beginning and
the end of the Veda.

Yo vedaadau swarah prakto vedantecha pratishtitah.

The Vedas frequently use the expression idam, atha, tat and etat. Idam
refers to that which is near, atha to that which is not so proximate,
and tat to that which is distant. In this context, it is worthwhile
noting the existence of an affinity between the various languages of the
word, a fact which we can understand when we examine the root or origin
of some of the words. Without entering into philological or other
controversies, it can be stated that in the distant past one culture and
one civilisation prevailed throughout the world. While that old culture
decayed and disappeared, or gave birth to a new culture and a new
civilisation in some parts of the world, they continued to exist and
flourish in other parts of the world. That culture and civilisation go
by the name of Sanatana Dharma. There is evidence to show that the Mitra
cult, Maitra-Varuna referred to in the Vedas, prevailed in certain parts
of Europe before the advent of Christianity. In some Far Eastern
countries, though the rulers are Muslim, Observances prescribed in the
Hindu Sastras for coronation are followed when a ruler is put on the
throne. Counting from the month of March or Chaitra, the first month
according to Hindus, it will be seen that September is the seventh
month, October the eighth month, November the ninth month, and December
the tenth month. For days of the week, the names of planets used in
India are adopted in other parts of the world also.

This is a small disgression. Now coming back to the subject, it should
be realised that the expression tat occurring in the Vedas refers to
Isvara. The Vedantic tatvam is the realisation of the swaroopa of
Iswara, or the Ultimate Reality. The plain meaning of tatvam is truth or
reality. The secrete of understanding this reality is contained in the
world tat-tvam, the realisation of tat or That as tvam or yourself. The
jnana-mudra, or the sign of the hand with the tips of the right thumb
and the index finger meeting, is an indication that Tat which appears to
be distant is within oneself. When we look at the horizon, we get a
feeling that at a distant point, the earth and the sky are meeting.
Suppose we decide to proceed to that meeting point. As we go on walking,
the supposed meeting point goes on receding further and further and
ultimately we will find ourselves back at the point from where we
started. In other words, the point from which we originally looked at
the distant meeting point on the horizon is also the point where the
earth and the sky meet.

There is an interesting story of a young woman who decided to marry only
the greatest person on earth, though her parents had selected a
bridegroom for her. She fixed the king as per object and when she
approached him for requesting him to take her as his wife, she thought
that a Sanyasi to whom the kind paid homage must be greater than the
king. Thereupon she left off the king and when after the Sanyasi. So the
story goes on and ultimately she came to the starting point and married
a common man, who turned out to be none other than the person whom her
parents had selected.

While Tat is a Ultimate Reality, the Upanishads proclaim that idam, or
that which is in our proximity, cannot exist without a root or origin -
Nedam amoolam bhavishyati. A tree sprouts from the earth, is sustained
by the earth, and is finally absorbed by the earth when it decays. All
the things we perceive with the aid of our five senses are connoted by
idam. The perceiver within us is the origin of the things perceived. As
the same electricity shines in different colours and with varying
brilliance according to the colour, size and powers of the bulbs, so too
the same Isvara is within all of us and looks through the window of our
mind at all things without, which are rooted in Him. The origin of diam
is jnana and that janan, though apparently confined to the mind of
individuals, is full and all-pervasive. The root of all things with
life, whether stationary or moving , is in that all-pervasive janana,
which is the Tat of the Vedanta. That is what the following verse in the
Gita also tells us :

> Avibhaktam cha bhooteshu
> Vibhaktamivacha stthitam;
> Bhoota-bhartru cha tat jneyam
> Grasishnu prabhavishnu cha

The Tat or That which is the Ultimate Reality achieved through jnana,
must be understoo9d as the Protector, Destroyer or Consumer, and the
Creator of all bhootas (elements like air, water and fire, and all
beings, moving and stationary), who appears divided between these
elements and beings, though He is invisible. This Tat is seen at the end
of the Vedas, and we realise that all-pervading Truth or God by
contemplating on a seeming part of that Truth. This is known as Isvara
dhyaanam or devotion for or contemplation of a particular manifestation
of God and is a process of learning to be ready to receive with both
hands the fruit of janana and Bhakti when the time is ripe for the fruit
to fall, namely, the Divine grace to descend. 

