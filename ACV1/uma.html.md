Title:Uma Paramatma Swaroopa

**UMA – PARAMATMA SWAROOPA**

The Upanishads are also known as Veda-siras, or the crown of the Vedas.
There are ten main Upanishads and one of them is Kena Upanishad. In this
Upanishad, a truth expounded by the Vedas is explained by means of a
story. According to this story, the devas once decided to celebrate
their victory over the asuras. At this festival, all the devas were
filled with a feeling of self importance and pride in their own prowess.
To cure them of their egoism, God appeared in the form of a Yaksham, a
bright apparition which touched the earth below and the heavens above.
The identidy of this phenomenon the devas were unable to comprehend.
Agni (fire) was sent to find out what it was. To a question from the
Yaksham, Agni said that he was Jatavedas having the power to reduce
anything and everything to ashes. Thereupon, the Yaksham threw in front
of Agni a blade of grass and asked him to consume it. Even though Agni
concentrated all his powers, he was unable to burn it. He came back
humbled. Similarly, Vaayu or Maatarisva also failed to move the blade of
grass, even though he concentrated all his fury to blow it off. Finally,
Indra, the Lord of the Devas, approached the Yaksham. The apparition
vanished and before the crest-fallen Indra stood the form of a damsel
whose lustre illuminated the entire place. She was no other than Uma or
Haimavati, the Divine Mother, from whom every one and everything derives
sustenance. This jyoti-swaroopa informed Indra that the Yaksham who was
present a while back was no other than Paramatma, the source of all
energy and life, and that if the devas had succeeded in conquering the
asuras it was due to the grace of that Paramatma. Indra became
enlightened and humble and he communicated this knowledge to the other
devas. The knowledge destroyed the demon of egoism from their hearts,
which then became pure.

Uma, the Divine Mother, is the personification of pranava (Om). She is
brightness in light and fragrance in flowers. She has the illumination
of a thousand suns and yet has the soothing coolness of a thousand
moons. Along with Isvara, she is the Paramatma-swaroopa sung by the
Vedas.

It is this idea that is conveyed by the following verse in Soundarya
Lahari :

> ***“Sruteenaam moordhaano dadhati tava yau sekharatayaa,
> Mamaapi-etau maatah sirasi dayayaa dhehi charanau;
> Yayoh paadyam pathah pasupati jataa-joota tatini;
> Yayor-laakshaa lakshmih-aruna hari choodamani ruchih-“***

In this stanza Sri Adi Sankara prays to the Mother (maatah), to place
Her divine feet, the feet which shine in the crown of the Vedas
(sruteenaam moordhaano), even on his head, in the plenitude of Her mercy
(dayayaa). The quality of the divine feet is explained in the last two
lines. The waters with which those feet are washed (paadyam), becomes
the river Ganges, flowing over the matted hair of Pasupati. The
beautiful lac colour of those feet is caused by the lustre of the red
crest-jewel of Hari. It is significant that Adi Sankara gives expression
to his humility by once again using the expression maamapi, even mine.
In another sloka in Soundarya Lahari also, he has used maamapi when
begging the Divine Mother to bathe even him with the glance of Her
soothing eyes – Snapaya kripaya maamapi sive.

Let us surrender ourselves at the Mother’s feet which the Vedas praise,
get purified of heart, and attain lasting bliss.

November 15, 1957


