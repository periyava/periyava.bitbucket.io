
**NEED TO WORSHIP DIVINE MOTHER**

In this world, we mortals are so overwhelmed with ajnaana (ignorance)
that though we know a thing to be wrong, we are helplessly impelled to
do it. Ajnaana is a disease for which jnaana (enlightenment) is the only
cure. The Divine Mother alone is capable of bestowing this milk of
jnaana (jnaanappal in Tamil), removing our ignorance, and satiating the
hunger of our atma (soul). A hungry child thinks of its mother and the
milk she will give and yearns for both. Similarly, we must yearn for the
grace of the Divine Mother, so that we can obtain from Her the milk of
enlightenment. For that purpose, we must be constantly thinking of Her
and praying to Her.

The time available to us, after attending to our prescribed and
essential duties, must be utilised in contemplation of the Divine
Mother. If we do not switch over our mind in Her direction, when we have
nothing else to do, there is the danger of the mind straying along the
forbidden or sinful path. If, on the other hand, we think of Her, we
will not only be avoiding doing wrong, but will also be fed by Her with
the milk of jnaana. As a result, we will be endowed with the grace of
Saraswati, the Goddess of Learning, of Lakshmi, the Goddess of Wealth.
No only that, physically we will be healthy and radiant with charm
(tejas) that flows from health. We will also be blessed with long life.

Thus, long life, health, wealth, and knowledge – what is the use of long
life bereft of health, wealth and knowledge ? – will be ouras. Jnaana
will sever the paasa (chord) ajnaana, which binds the soul to this world
and makes us pasu (animal). When the bond is sundered, the liberated
soul merges itself into that limitless and all-pervasive Bliss.
Parananda, and is no longer afflicted by fear, sorrow, or pain. Thus,
the effect of worshipping the Divine Mother is the fulfillment of the
purpose of life – the merger of the atma with the Paramatma. This is the
significance of the following verse in Soundarya Lahari, which is given
as the phalasruti for the 100 verses preceding it.

“Sarasvatyaa lakshmyaa vidhi-hari-sapatno viharate Rateh paativratyam
sithilayati ramyena vapushaa Chiram jeevanneva kshapita pasupaasa
vyatikarah Paraanandaabhikhyam rasayati rasam tvadbha-janavaan”.

January 31, 1958


