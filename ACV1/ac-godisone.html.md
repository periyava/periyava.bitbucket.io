Title:God Is One

 
 

### God is One

We Hindus regard both Siva and Vishnu as the same and this is evident
from the fact that in the ecstasy of our devotion, whether were are
alone or are in groups, we exclaim " Haro-Hara" and "Govinda-Govinda",
which(whose) names come to our lips spontaneously. The holy days of
Sivaratri and Janmashtami are divided from each other by exactly 180
days, and this seems to indicate that God in His aspect as Siva protects
us during one-half of the year, and in His aspect as Vishnu, in the
other half. The traditional practice of boys and girls collecting oil
for their vigil on Sivaratri and Janmashtami nights, singing in chorus a
song which means that Sivaratri and Sri Jayanti are the same, is another
pointer to the identity of these two manifestations of the Divine.
Apachaaranivrtti must precede Anugraha - eradication of sins must
precede blessings. So God as Hara destroy the sins of His devotees,
while as Govinda, He protects them from harm. The expressions Hara -Hara
and Govinda-Govinda come to children effortlessly. It is significant
that Sri Sankara composed Bhaja Govindam when he was a child and Sri
Sambandar sang that Hara naama should envelop the world, when he too was
a child. The Upanishads speak of God as Uma-sameta-Parameswara, and it
is worthy of note that all children refer to God as Ummachi, which is
obviously a contraction for Uma-Maheswara. Thus, in the language of
children, there is no difference between Siva and Vishnu.

The sense of religious toleration is not a modern conception. It can be
traced to very ancient times. The Kural proclaimed that all teachings
referred only to one Porul or Object. Sri Sankara and Sri Sambandar saw
the same God worshipped in the six systems to which they referred.
Arhat, the name by which Jains call the Supreme Being, is a Vedic name
identified with Siva. Other religions also speak of one God.

All troubles in rthis world start only when attempts are made to wean
away people from their native religion to convert them to a new faith,
by holding out the temptation that people can attain salvation only if
they embrace that new faith. This is more than what any sensible person
can swallow. Since every religion speaks of God, to ask a person to give
up the religion in which he is born is tantamount to asking him to give
up God and is a sin against God. It is the duty of every person to
follow the religion of his ancestors. If a non-Hindu finds that he had
Hindu ancestors, its up to him to revert to Hinduism after performing
the prescribed Praayaschitta(purificatory ceremony).

While there is propaganda for other religions, there is none for the
Hindu religion. Propaganda is a prescribed duty for other religions,
while in respect of Hinduism, it is enjoined that one should not tell
unasked-Ma Aprshtah Kasyachit Brooyat. It is noteworthy that so may
continue to profess Hindu religion even without preaching and
propaganda. The cause of the stability of Hindu religion is that each
practised his prescribed Dharma. If each person does his appointed duty,
then our religion will be strengthened both in its Vedic foundations and
in its ceremonial practices. It is only that way the Vedic religion has
survived down the ages.

October 22, 1957. 

