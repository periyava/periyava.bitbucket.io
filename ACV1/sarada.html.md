Title:Sarada Navarathri

**SARADA NAVARATHRI**

**CONCEPTION OF PARASAKTHI**

In Mooka Panchasati, Sri Kamakshi is referred to as darkish blue in
colour in the Stuti Sataka, and as saffron in colour in the Aarya
Sataka. Sri Sankara Bhagavatpada, in his Soudarya Lahari, describes
Ambika’s colour as aruna varna, splendrous red of the rising sun. Why is
the colour of the same Goddess described as dark-blue in one place and
red in another?

According to Devi Mantra Sastraas, Kameswara, who transcends the
Trininty, Brahma, Vishnu, and Siva, is actionless and unattached. Sri
Kameswari, the Supreme Parasakthi, seated on the left of Sri Kameswara,
is described to be red in colour. The sameness of Parvati, who is dark,
and Paraasakti, who is red, is indicated in Mooka Panchasati by
attributing both these colours to the Supreme Goddess.

Sri Kameswara’s swarupa is like that of a pure sphatika, a colourless
solid which becomes invisible when immersed in water. He is thereby
conceived as formless even though He has a form. Vishnu and Paravati,
both dark-blue, are twin manifestations, are also Siva and Saraswati,
both white, and Brahma and Lakshmi, both golden yellow. Daylight is
colourless, and yet it contains all the primary colours. If one of the
colours is separated from the colourless light, the rest of the colours
reveal themselves. Red is the least disturbing colour as is evident from
the fact that red light is used to develop photographic negatives. Under
the influence of red Paraasakti, the colourless Sadasiva manifests
himself as Brahma, Vishnu, Siva and their respective consorts,
Saraswati, Lakshmi, and Parvati and starts the activities of the
world-process.

The Divine Trinity have their counterparts in the three states of
wakefulness, dream, and dreamless sleep, and in the three-fold
activities of creation, protection, and dissolution. While wakefulness
and dream are states of mixed joy and sorrow, sleep is a state free from
worldly sorrow. Turiya is a state higher than sleep and that is the
state of Supreme Bliss or aananda. Pralaya or deluge gives rest to souls
from the cycle of birth and death and the effects of punya and paapa.
Siva, who is referred to as the Destroyer, is in reality a merciful God,
who lulls the tormented souls into the sleep of pralaya, during which
period they forget all their sorrows. In Soundarya Lahari, Sri Sankara
Bhagavatpada says that these three Divinities, Brahma, Vishnu, and Siva,
started their cosmic process when the Supreme Parasakti knitted Her brow
for a fraction of a second. She stood beyond them all and Her red
splendour stimulated them to perform their respective functions of
creation, preservation and destruction, by Her very presence.

------------------------------------------------------------------------

As part of Sarada Navarathri celebrations, articles included in the book
“Acharya’s Call”, complied by Sri V. Ramakrishna Aiyer, Retired Chief
Reporter, The Hindu, Madras, and published by Sri Kamakoti Peetam, Sri
Sankaracharya Swami Mutt, Kancheepuram, are reproduced for the
information of devotees.

> ***“Jagat soote dhata harir avati rudrah kshapayate
> tiraskurvan etat swam api vapur eesastirayati;
> sadaa poorvah sarvam tadidam anugrnhati cha sivah
> stavaajnam aalambya kshana chalitayor-bhroolatikayoh.”***

The lesson to be drawn from the foregoing is that the same Supreme Being
appears in diverse forms as we conceive Him to shower His grace in the
manner we invoke it. We do it by mantra and japa which are sound waves
having the power to transform themselves into the form of the murtis
whose mantras they are. If we continuously chant the mantra into which
we are initiated, the Supreme Parasakti will shower Her grace on us. She
is meditated in the moon which gives soothing light and also assuages
heat. Thus She sheds Her nectar rays all round. The Para Devata whom we
worship and the Full Moon we see in the sky are related in this life. It
behoves us, therefore, to constantly meditate on any chosen mantra on an
Ishta devata so that our soul may be enveloped by that Devata with that
mantra on our lips, even at the time when the soul departs from the
body. That is the path shown to us by our sages and all of us should
pursue this path in the interest of universal welfare.

December 8, 1957


