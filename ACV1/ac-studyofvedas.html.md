Title:Study Of The Vedas

 
 

### Study of The Vedas

The state of things in this country for the past 100 years or so is such
that the Vedas are being brought to the notice of the people of this
country, the land of the Vedas, through the published works of Western
Orientalists. While we should acknowledge with gratitude the invaluable
contributions made by these research-scholars of the West in
classifying, printing and preserving the Vedas, so far as we in India
are concerned, the primary purpose the Vedas, namely to memorise and
recite correctly, with proper accents, or adhyayana, cannot be served by
these publications. The Vedas printed and preserved in libraries will
(in the absence of regular study and recitation) eventually acquire only
a museum value, and the future generations reading these published works
may marvel at the wonderful things contained in them.

The Vedas are intended to serve a different purpose. They have to be
learnt by heart, understanding the correct way of pronouncing the
mantras by listening to the rendering of the mantras by the guru
(teacher). The Veda mantras so learnt should become the guide in our
daily life, in our Karmaanushtaana, Tapas, Isvara aaradhana, etc. If, in
India, the Vedas retain their original vitality even today, it is
because these hyms are being continuously repeated by students and
teachers of the Vedas, and the purity of the sounds and accents of the
words are retained in that process. It is only by practising the Vedic
injunctions that we can obtain the grace of God, both for our individual
welfare and for the welfare of the whole world. That is why the mere
preservation of the Vedas in well-bound volumes cannot secure us the
benefits for which they are intended.

In fact the Vedas are never intended to be written down and read. Veda
Adhyayana implied hearing from the lips of the teacher and repeating
after him. That is why in ancient Tamil classics, the Vedas are referred
to as Ezhutaakkilavi , unwritten book. Veda Paatakaas, who learn from
books, are included among the six classes of inferior scholars. The
other five classes are those who recite the Vedas musically, those who
recite very fast, those who shake their heads while reciting, those who
do not know the meaning, and those who have a poor voice. This is made
clear in the following verse :

> Geetee seeghree sirahkampee
> tatha likhita paatakaha
> narthajnah alpakanthascha
> shadete paatakaadhamaah.

The study of the Vedas has been confined in these days to a few
professionals (purohits), who are not even accorded a proper status in
society. Many of them learn mechanically, without caring to understand
the meaning of the Vedic hymns. At this rate, there is a danger of Veda
adhyayana becoming extinct, in the not so distant a future. To avert
such a situation, a private Trust has been formed with land gifts made
by some donors for encouraging the study of the Vedas in the traditional
manner. About six years ago, statistics were prepared of those who have
made a complete adhyayana of the Vedas, in each saakha (branch), and
presents were given to them. Stipends are being given to qualified
students in each saakha, who desired to learn Veda-bhaashya under a
competent teacher. Half-yearly examinations are being held in the
prescribed Veda and Veda Bhaashya portions and Sambhaavanaas are being
given to successful candidates.

There are many people who genuinely regret that they have to perform the
various rituals prescribed in the Saastras without understanding the
meaning of the Mantraas employed therein. Take the instance of the
marriage ceremony. The indifference shown by our young men and women for
the rituals connected with marriage is due to their ignorance of the
meaning of the Mantraas, they are asked to repeat. If the meaning of the
Mantraas are explained to them beforehand, by a competent person, they
will be able to go through the rituals with better understanding and
greater devotion. The same procedure can be adopted in respect of
Upanayana and other Samskaaras.

Veda adhyayana, without knowing the meaning thereof is like preserving
the body without the soul.

Veda Mantraas uttered with a knowledge of their meaning will lead to
Paapa-parihara(expiation of sins), and Arishtasaanti(liquidation of
evil), and pave the way to Brahma-saakshaathkaara(God realisation).

The Vedas are the roots of our religion. All other Paraphernalia, like
feast and festivals, are like the leaves and fruits of that tree,
depending for their sustenance on the Vedic roots. Though imbedded in
mud, the internal core of the roots is as fresh and fragnant as the
fruits and flowers on the top. It is no use feeling gratified that the
Vedas have been written down, printed and published by Western Scholars.
To us, Veda adhyayana and their employment in the sacraments of our
daily life are important. For that purpose it is necessary to learn them
by heart, understand their meaning, and recite them in the prescribed
manner.

February 1, 1958. 

