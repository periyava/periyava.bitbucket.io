Title:The Sanctity Of Sastras

 
 

### The Sanctity of Sastras

The ancient Hindu empires of this land were built on the foundation of
Vedic religion and all great Hindu rulers took special interest in
upholding Hindu Dharma and encouraging savants who dedicated themselves
to the task of increasing human knowledge, both in the material and in
the spiritual spheres. The result is that we have inherited a veritable
treasure of knowledge in the shape of various Sastras.

Instances can be quoted to show that the empires which flourished in
different parts of Bharata Varsha (India) rose on the firm foundation of
Vedic tatavas (principles). Vikramaditya built up a mighty empire about
1,500 years ago. Scholars, well-versed in different branches of
knowledge, flourished under his patronage. Among them were Kalidasa,
Bhattabana, Varahamihira and six others, known to the outside world as
the nine gems who adorned the court of Vikramaditya. Varahamihira is the
author of several works, among which Brhat samhita is one. In it, he has
condensed all the Sastras and devoted one chapter for each Sastra. He
has also written a Naadi Sastra, dealing with the science of breathing
or praanaayaama. He has proved therein how the character of a person is
influenced by the manner of his breathing, and also dealt with the
control of breathing, which is conducive to longevity and which also
helps to awaken the power of the kundalini, and thereby enables a person
to acquire spiritual powers step by step, ultimately leading to the
realisation of Brahmam.

The kingdom of Bhoja was also founded on Vedic tatva. The modern Dhar
was his capital. He was a poet himself and in the capital none but poets
lived. There is a story about a weaver who was pursuing his hereditary
avocation in this capital town. It is said that one day the servants of
Bhoja took him to the court of the king. When he was asked whether he
could compose poetry, he is stated to have given his reply in the
following verse.

> Kaavyam karomi nahi chaarutaram karomi
> yatnaat karomi yadi chaarutaram karomi
> Bhoopaala-mauli-mani-mandita paada peetthaHe
> saahasaanga kavayami vayaami yaami

The import of this verse is that this weaver told the king that he can
compose verses, but not well, though he can compose good verses with
some effort. Punning upon the word yammi, he told the king that he
composes, kavayaami, that he weaves, vayaami and then goes away saying,
yaami. According to the story the king rewarded him generously. The idea
sought to be conveyed by this story is that any one coming to the court
of Bhoja became a poet. Archaeologists exploring the site of a former
masjid in Dhar discovered a chakra (wheel) with the principles of
Sanskrit gramme inscribed on it. In Bhoja's time also, great savants
wrote invaluable works containing truths which human intelligence was
able to unravel.

Bhoja has himself written a book on vimaana or aerial transport. This
work has been printed in Baroda. It deals with all the secrets of aerial
travel, but does not say how to construct a vimaana or aeroplane. It is
not as if Bhoja did not know the technique of constructing a vimaana.
But this knowledge was kept secret, to safeguard against any possible
misuse of the technique by evil-minded persons with calamitous results.
In this context it is noteworthy how the perfection of long range
rockets in the present day is causing anxiety to nations and how public
opinion is shaping towards the control of their production and use. The
principle of vimaana construction in those days appears to be based on
rasa vaada (alchemy). I am mentioning this to show that several
scientific works have been written by ancient thinkers. But due to
neglect by people and to historic reasons, many such works have been
lost to us.

Coming to later times, we find that the jurisdiction of the Vijayanagar
empire extended up to Kanyakumari. The one person who helped to found
and build up this great Empire was Vidyaranya, a Sanyasi. He is the
author of Veda Bhashya, commentaries on the Vedas, and several
philosophic works. The Vijayanagar empire was also built on the
foundation of our religious principles. Generals like Gopanna did yeoman
service in the protection and construction of numerous temples. When
danger threatened the temple of Sri Ranganatha at Srirangam, Gopanna
gave protection to it. Sri Vedanta Desika has composed a verse
expressing gratitude to Gopanna for this service. Kempanna, who led a
expedition to the South and annexed territories for the Vijayanagar
empire, was responsible for the renovation of several temples and tanks.
His wife, who accompanied him during this expedition, has recorded all
this in a Kaavya known as Madura Vijaya. In this way, Sri Vidyaranya
helped to build up this empire on the sold strength of our Sastras.

The Maharashtra empire founded by Shivaji has also a similar religious
foundation. Samartha Ramdoss, regarded as an incarnation of Sri
Anjaneya, contributed spiritual strength to Shivaji, and also functioned
as his counsellor. Between them they accomplished great things. There is
none to equal Sri Anjaneya in intelligence, valour or devotion. Whenever
Sri Rama's name is uttered in devotion, there is present Sri Anjaneya,
shedding tears of joy and devotion, says the verse :

> Yatra yaTRA Raghunatha keertanam
> Tatra tantra Krtamastakanjalim;
> Baashpa-vaari paripoorna lochanam
> Marutim namata raakshasaantakam.

By worshipping Sri Anjaneya we will be endowed with intelligence,
strength, yasas, courage, fearlessness, health, gift and speech, etc.

> Buddir-balam yaso dhairyam
> nirbhayatvam arogata,
> Ajaadyam vaak-patutvancha
> Hanoomad smaranaat bhavet.

Let us try to acquire these qualities by worshipping Sri Anjaneya.

During the reign of Chola kings alos, wonderful achievements have been
made on the strength of religion; and people made enormous progress in
Government, arts and education.

Some people are inclined to test the correctness of the teachings of a
religion by the yardstick of science. But we have seen how later
investigation and research have disproved earlier scientific theories.
Therefore, how can the shifting theories of science establish the truth
or otherwise or a religion ? On the other hand, our Sastras have stood
the test of times. Therefore, we must try to understand the Sastras that
are in vogue in our country. If we are unable to understand any Sastra,
we must not reject it and allow it to disappear. It is our duty to at
least preserve it so that posterity may profit by it. There are many
truths in our Sastras which science has not yet succeeded in unraveling.
We speak of the seven wonders of the world; but have we paused to study
the wonderful achievements of our forefather ? There are in our temple
architecture many features, which modern engineering knowledge has
failed to explain. What about the Kutub Minar and the iron pillar at
Delhi which have withstood exposure to the weather for over 1,000 years
? Do they not proclaim the skill of our ancestors in forging iron ?

Kingship has given place to democracy and therefore it has become the
duty of the people themselves to preserve the treasures lying imbedded
in our Sastras. Instead we are frittering away our energies in
linguistic and other controversies. This is a feature of our present-day
life, which is very sad to contemplate. Last year, we became
apprehensive of the very future of this country, on account of the
virulence of the controversy over re-organisation of States. It kings
fought amongst themselves in the past, people are forming themselves
into groups and fighting each other in the present. There is also the
ideological conflict among the nations of the West. It is desirable that
all these controversies should end and peaceful progress of humanity is
assured. So far as we, in India, are concerned, we should devote some
attention also, in the midst of our other pre-occupations, to the study
and the preservation of our Sastras. We should approach these Sastras
not in a spirit of scorn, thinking that in the modern scientific age
they have nothing to teach us, but, in a spirit of devotion of a seeker
of truth. May Sri Anjaneya dower us with the necessary strength, courage
and wisdom to pursue the study of our Sastras, and may He bless us all
with happiness. 

