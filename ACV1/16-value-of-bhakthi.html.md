Title:Value of Bhakthi

### Value of Bhakthi

> Karalagna mrgah kareendra bhango
> Ghana-saardoola vikhandanosta  jantuh:
> Giriso visadakritischa cheetah
> Kuhare panchamukho-asti me kuto bheeh.

Lord Narayana made up His mind to remain as a man when He incarnated as
Rama, in order to teach the world the importance of reverence or Bhakti
towards father, mother, teacher and God. He so identified Himself with
His human role that He behaved exactly like an ordinary mortal and when
any one attributed to Him qualities of God, He reminded him that He as
only a man – *Aatmaanam maanusham manye*. Similarly though Sri Adi
Sankara was Lord Siva incarnate, he tried to inculcate Siva-bhakti in
the people by his actions and writings. One such composition of his is
*Sivaananda Lahari*.

 

In the above verse occurring in Sivaananda Lahari, Siva is conceived of
as having five faces, four of them looking at each of the four
directions, east, south, west and north, and the other turning upwards.
The upturned faced is called *Eesaanam,* while other four faces are
called *Sadyajaatam, Vaamadevam, Aghoram*, and *Tarpurusham*. Siva holds
in his right hand a deer (hence *valam-kai-maan* in Tamil), symbolic of
the mind which is unsteady or restless. This aspect of the mind is found
referred to in the Gita in the words, *chanchalam hi manah* *Krishna.*
The deer is also by nature restless and timid and continuously turns its
gaze hither and thither. But when the same deer is held in the hand of
Siva, it gazes into His benevolent eyes, keeps its look steady there,
forgets its fear and remains motionless, with a feeling of security and
happiness. Siva wears the hide of an elephant and that of a tiger. In
the atmosphere of peace and security that pervades in His presence, all
creatures remain motionless and blissful, their mind concentrated on Him
and Him only. Where is fear, asks the great Acharya, when this five
faced Siva is in the cavity of my heart?

 

There is an interesting story about the manner in which Siva came to
wear the hide of an elephant. It is said some sages who believed that
the observance of the rites prescribed in the Vedas is everything and
that there is no need to have devotion or *bhakti* to God, created an
elephant by their mantra power and set it to attack Siva, towards whom
the wives of the sages were attracted, even as Sri Krishna attracted
towards himself the devotion of the *Gopis*. *Isvara* performed his
*Oordhva Taandava,* tossed the elephant about like a ball and ultimately
tore it up and covered Himself with its skin. On account of this dress,
He came to be known as *Krithivasah*. The Vedas use the expression,
(*Krithivasahpinaakee*) in several places, Amara Simha, a highly
intellectual person, though a Jain has done full justice to the Vedic
names of God in his Sanskrit dictionary. When enumerating the names of
Siva, he has included the Vedic name, *Krithivasah*.

 

There is a story about the meeting between Sri Adi Sankara and Amara
Simha. Both Jainism and Buddhism expounded only truths which are within
the comprehension of the intellect. Adi Sankara was able to convince
Amara Simha that the Ultimate Reality or *Isvara Tatva*, is something
beyond the reach of mere intellectual comprehension. Amara Simha
thereupon started consigning all his writing to the flames. Adi Sankara
rushed forward to prevent him doing so, but was in time only to save
*Amara* Kosa, which has become a book of eternal value.

 

The Gita also teaches us that the Vedas and the rites enjoined therein
are not the be all the end all of our spiritual quest, but that there is
also the Vedanta or the highest conception of the Supreme which
transcends the intellect. It is up to us all to develop *Isvara-bhakti*
and derive happiness herein and hereafter.

 

The description of Siva, the Lord of the universe, in this verse, can
also be applied to the lion, the Lord of the jungle, *Panchaasya* or
*panchamukha* is one of the names for the lion, derived from the fact
that its head and mane together present a broad (*pancha*) appearance in
contrast to its wiry body. While roaming about, the lion catches hold of
deer with ease and also kills the elephant or tiger that corsses its
path. Its den is known as *kuhara*, and when it is prowling about, the
other animals of the jungle remain hidden and motionless.

 

**November 4, 1957**
