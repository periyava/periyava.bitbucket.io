Title:The Age Of Vedas

 
 

### The Age of Vedas

The sacred books of Buddhism, Christianity and Islam have definite
historical dates assigned to them. The Tripitakas are said to have been
written about the time of Asoka , though the Buddha's date , according
to some puranas, went further back by several centuries. The Koran,
which is the word of God conveyed to the Prophet, is about 1,200 years
old. The New Testament of the Bible is about 2,00 years old. Though no
one can assign any definite date to the Vedas, Oreintalists are anxious
to discover when they were "composed". Some of them say that it was done
about 1,500 BC; other suggest that it may have about 3,000 years. Tilak
fixes the date as 6,000 BC But modern Orientalists are inclined to bring
the date nearer.

If the Buddha was born 2,500 years ago , and if in the Buddha's time it
was not known when the Vedas came into existence, then the date of Vedas
should be long anterior to this time. But the truth of the matter is
that the Vedas are Anaadi and Nitya, with out beginning and with out end
and eternal , like sound. They manifest themselves after each Pralaya
(deluge). Pralaya and srshti (deluge and creation) alternate The Old
Testament speaks of the creation of the present world. According to
Hindu Scriptures, there have been many creations before, and also
Pralayas. The period of each Srshti and each Pralaya spreads over aeons
, infinitely beyond human calculation. The findings of geology, which
traces the history of the earth to a period long before the time
referred to in the Book of Genesis, confirm this view of cycles of
creation and deluge. In fact, the more and the deeper the researchers of
modern science, the greater the confirmation for the declarations in our
Hindu scriptures. I t is futile, as it will be foolish, to discard these
declarations at any time , on the ground that they do not accord with
the discoveries of science known till then. For, fresh light thrown by
later scientific discoveries provide startling confirmation for many of
these declarations.

Orientalists also attempt to fix the date of the Vedas on the basis of
certain internal evidence about the relation between the Sun and the
stars, etc. The Hindu theory of cycles , however, refers to several
creations an d so, the same astronomical coincidences and deviations may
have occurred during the period of some past Srstis also. We do not know
how many such Pralayas and Srshtis have gone before.

Another method adopted for fixing the age of the Vedas is to go by the
changes in the style of Hindu scriptures, from the Rik Samhita down to
the Kaavya literature. In the case of spoken languages , it has been
computed that gradual mutations took place with the passage of every 200
years. For example, the Tamil language to-day is different from the
Tamil of the Sangam age. It is far cry from modern English to old
English. American English is different from the orthodox English in use
in Britain. On this method of appraisal, it has been suggested that the
Vedas should have come into existence 1,500 years ago.

It is common knowledge and experience that if a thing is in constant
use, it will wear out and bear marks of such use and wear, and that a
thing rarely used . the same is true of languages also. English, Tamil
and Hindi have changed in from through the centuries and undergone even
distortions by usage. But the language of the Vedas remains to-day the
same in form and feature as it was at time out of memory. The reason for
this absence of distortion or deterioration is that Vedic chanting has
been so carefully guarded, as not to allow any possibility of a lapse
from its pristine form. Of the innumerable Veda Saakhaas, we know now
only of one out of the 21 branches of the Rig Veda, three out of the 101
branches of the Yajur Veda, two or possibly three out of 1,000 branches
of the Saama Veda, and on e only out of 11 branches of Atharva Veda, one
will have to devote to its learning about eight years, night and day.

There are various methods in Vedhaadhyayana (memorising the Veda
mantras) which help to preserve the number an d the order of the words
and letters of each Veda Mantra. There are specifications regarding the
time interval (matra) for the utterance of each letter in a word; the
part of the body from which each of the sounds in the word should
emanate by the exhalation of the breath in an appropriate manner ; the
affinities between the Swaraas in the Vedas and the Swaraas in Sangeeta
(music) and the affinities of both (Veda Swaraas and Sangeeta Swaraas)
to the natural Swaraas in the sounds produced by animals , birds, etc.
These methods of memorising the Veda Mantras are known as Vaakya, Pada,
Krama, Ghana, Jata, Swara,etc.

The Vedas are not like the sounds of common speech to undergo periodical
changes by usage; on the other hand, they have been meticulously
preserved as a result of being protected by definite prescriptions and
indices relating to the sound measures, their nature, sequence, manner
of utterance, etc., which have been preserved by oral transmission from
generation to generation. The injunctions relating to the persons who
should do the Adhyayana, and the Aachaaraas such persons should observe,
are intended to promote this objective. To approach the study of the
Vedas with out a clear knowledge of all ramifications and to adopt rough
and ready methods for estimating their age will amount to proceeding on
a basis which has no bearing on the subject of the study.

January 28, 1958. 

