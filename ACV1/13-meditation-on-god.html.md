Title:Meditation on God

### Meditation on God

 

> Naaraayanaaya nalinaayata lochanaaya
> Naamaavaseshita mahaabali vaibhavaaya
> Naanaa charaachara vidhaayaka janmadesa
> Nabheeputaaya purushaaya namah parasmai

 

This verse occurs in Bhoja Champu and describes vividly the picture of
Sriman Narayana as He appeared to the Devas. This verse forms part of
the Ramayana story composed by King Bhoja and Poet Kalidasa, at a time
when King Bhoja was made aware of the fact that he had only ninety more
minutes’ life in this world. This is a beautiful verse and contains rich
ideas. The milky white ocean and the white *Adisesha* bed provide the
necessary relief or background to the scintillating dark-blue body of
*Narayana*. The very fact of His slumber is described as active vigil in
the protection of all the worlds and their contents. It is this
apparently dormant energy, which makes the entire universe function
according to plan, that burst out into a dynamic force in the form of
*Narasimha*.

 

King Bhoja tricked poet Kalidasa into reciting his (Bhoja’s)
*Charamasloka,* elegy on the death of King Bhoja. Hearing that inspired
poetry, King Bhoja, who was then in disguise, fell down dead. At the
entreaty of Kalidasa, the Divine Mother enabled King Bhoja to live
another ninety minutes. King Bhoja, when apprised of the situation, did
not feel sorry for his imminent death, but decided to utilise the brief
retrieve vouchsafed to him in singing the praise of the Lord. That is
how the concise *Ramayana* containing the above verse came to be
composed.

 

The moral is that we should employ even the few minutes of leisure we
may be able to snatch in between jobs in the thought of God or in
reciting His *naama*.

 

**October 25, 1957**
