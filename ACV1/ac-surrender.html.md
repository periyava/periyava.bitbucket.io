Title:Surrender To God

 
 

### SURRENDER TO GOD

No doubt, it is to some extent desirable, in this world, for a man to
earn a name and fame and also material wealth. All these things come to
some people unasked. Others do not get them, however much they may try.
But these things do not attach themselves to us permanently. Either we
leave them behind, or they desert us in our own life-time. Therefore,
name, fame and wealth are not objectives for which we should consciously
strive with all our energy. What we should aspire and strive for is a
life free from sin.

There are two aspects to this freedom from sin. One is absolution from
sins already committed (Paapanaasam) and the other is non-commission of
sins hereafter, by purifying our mind and making it free from evil
thoughts (Paapa buddhi). The former can be achieved by absolutely
surrendering oneself to God, realising that He alone is our Saviour,
nothing happens without His knowledge, and that whatever happens to us,
good or bad, is by His will and only for our ultimate good. Resigning
oneself to the dispensation of God is the essence of absolute surrender
or Saranaagati. We will be free from evil thoughts hereafter only by
Bhakti or devotion, that is to say, by devoting every free moment of
ours to His thought or repeating His names or listening to His glories.

The claim of Christianity is that God appeared on earth to wash off our
sins. It is sometimes argued that there is no corresponding conception
in Hinduism. This is not correct. In the Gita, Sri Krishna has given an
assurance that He will absolve from sin those who surrender themselves
to Him. The Lord says

> Sarva dharmaan parityajya
> maamekam saranam vraja;
> Aham tvaa sarvapaapebhyo
> mokshayishyaami maa suchah.

Sri Krishna asks Arjuna not to grieve telling him "I will free you from
all sins (Sarvapaapebhyo mokshayishyaami), if you give up all other
Dharma (Sarva Dharmaan parityajya), and surrender to Me absolutely
(Maamekam Saranam Vraja)". In this context, the import of the
expression, Sarva Dharman Parityajya has to be understood correctly. The
emphasis of the Gita is on each man performing his prescribed duties in
a spirit of dedication. Therefore, the call to " give up Dharma" cannot
be a message of inaction. Sri Krishna wants Arjuna, and through Arjuna
all of us , to do the duties pertaining to our station in life. But what
He wants us to give up is the notion that the performance of these
duties will by itself lead us to the cherished goal. Sri Krishna wants
us to perform our Dharma, giving up the notion that they are the be-all
and end-all of life, and surrender ourselves to Him without reservation.

In the verse previous to the one I have just quoted, Sri Krishna says :

> Manmanaa Bhava Madbhakto
> Mayaajee maam namaskuru;
> Maamevaishyasi satyamte
> partijaane priyosi me.

When Sri Krishna says to Arjuna, "You are dear to me(priyosi me) it
means that all of us are dear to Him. So, when he gives the assurance
"satyam te prattijanne", we can take it as an assurance given to all of
us . The assurance is that we will reach Him (Maamevaishyasi). For that
purpose, we have to fix our thoughts on Him(Manmanaa Bhava), become His
devotees(Madbhakto Bhava), worship Him (Madyaajee bhava) and fall at His
feet(maam namaskuru).

If we analyse one's affection towards one's son or wife, we will find
that it ultimately resolves itself into one's love for oneself. A man is
fond of his son only so long as that son fulfils what he expects of him.
Supposing that son gets married and neglects his father, the affection
will turn into enmity. It follows that the affection we entertain is
with a purpose and not selfless. But there is no purpose or object
behind one's love for oneself. When we come to realise that the "I" we
love so much is "He", our mind becomes saturated with Him. That is the
significance of the expression, "Manmanaa bhava". We think of Him not
for securing any favours, but because we cannot help thinking of Him,
having realised that the soul within us is none else than He. When this
realisation takes deep root, the mind enters the state of Avyaaja
Bhakti.

We have examples of such selfless devotion to God in our Puranas.
Akroora and Vidura had such Avyaaja Bhakti, Dhruva and Prahlada are
examples of those who surrendered themselves to God even from their
childhood. Sabari and Kannappar are examples of persons regarded as
unlettered common people, on the bottom rungs of the social ladder, Who
are animated by an overwhelming devotion in which the consciousness of
their individual entity has been completely wiped out. Parikshit is an
example of one, who, within the last seven days of his life, experienced
the bliss of devotion achieved in a life-time. Khatvaanga is an example
of a person who got purified by concentrated devotion of three and
three-fourths Naazhigas, or 90 minutes.

While Saranaagati helps to "write off" past sins, Bhakti alone will keep
our minds away from sin. The heart has to be kept clean through Bhakti
so that the full effect of His presence there may be realised. In the
ultimate analysis, surrender and devotion are the two facets of the same
thing. In this life, all householders are engaged in various occupations
necessary to maintain themselves. While so engaged, their minds will be
concentrating on their work. But it is during their leisure that their
minds are likely to go astray. This leisure must be utilised in
developing Bhakti, through various process like Naama Japa(repeating
God's name), Satsanga(keeping holy company), pooja(worship),
satkathasravana(listening to Lord's glory), etc. The idea is to somehow
keep our thoughts engaged on God. We should have no occasion to commit
sin through mind, eyes, ears and speech. Even when we make any
representations in our prayers, it should be in a spirit of detachment,
namely with the realisation that nothing is unknown to Him and with a
feeling, "Let Him do with us as He pleases". Let us, in this way, strive
to pursue the path of surrender and devotion, and earn the grace of God.

February 28, 1958 

