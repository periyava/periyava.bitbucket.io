Title:Adherence To The Sastras (Ancient Scriptures)

 
 

### Adherence to the Sastras

According to our tradition, there are fourteen branches of knowledge
which are common to both general education and dharma. They are
described as the sources of vidya and dharma. (Vedaah sthaanaani
vidyaanaam dharmasyacha chaturdasa - the fourteen (chaturdasa vidyaas
are, the four Vedas, the six Vedaangaas and the four Upaangaas. The six
angaas (limbs) of the Vedas are: siksha vyaakaranam, cchandas, niruktam,
jyotisham and kalpam). The Tamil expression, sadangu is derived from
shadanga or six angaas. The four Upaangaas are: the puraanaas, which
illustrate Vedic truths through stories-projecting the truths as if
through a magnifying lens-nyaaya, meemaamsa and the smritis. The smritis
deal with the Dharma Saastra portion of the Vedas. Between the
vedaangaas and the Upaangaas, almost all branches of knowledge are
covered. The jyotisha saastra, one of the Vedaangaas, covers the entire
field of astronomy, astrology and the technique of prediction. A
detailed study of this saastra will prove that our ancient Rishis had
perfected what is now known as higher mathematics, long before the
science of mathematics, was developed in the West. The Tamil expression
saangopaangamaha i.e., with angaas and upaangaas, used to denote a work
well done, is very significant. It coveys to us the idea that a job has
been done correctly and well, without forgetting even the minutest
details.
From the travel records written by Fa Hian and Huen Tsang, from the
various records available in China and from archaeological excavations,
we know the manner in which the ancient universities of Taxila and
Nalanda had been functioning. It is seen that though these universities
flourished in the heydays of Buddhism, all students were required to
study first the chaturdasa vidyaas which included the Vedas also.
Studies pertaining to Buddhism, of course, followed. I am mentioning
this to show how these fourteen branches of knowledge have been regarded
as basic for any education worth its name and for dharma.

Besides these chaturdasa vidyaas, there are four other branches of
knowledge, known as upavedas, in the scheme of general education. Thus
the number of branches of knowledge included for study in the scheme of
general education becomes eighteen. These upavedas are: Ayurveda,
science of medicine and surgery, which is stated to have originated from
the Rig Veda; Dhanurveda, including physical culture and military
science, originating from the Atharva Veda; Gandharva Veda, which is a
term used for all fine arts, including music, dancing, painting, and
sculpture, originating from the Sama Veda; and Artha Sastra: the science
of politics and administration, having its origin in Yajur Veda.

Sri Harsha, in his Naishadha, has punned on the word, chaturdasa, when
describing the education of Nala. He says:Adhiti bodha aacharana prachaaranaihDasaaschatasrah pranayan upaadhibhihChaturdasastvam kritavaan kutasvayamNavedmi vidyaasu chaturdasasvayam

The poet says in this verse that Nala made the chaturdasa vidyas into
chaturdasa. Dasa, in the second chaturdasa, has to be given the meaning,
"stage". The verse says that Nala's education in all the fourteen
branches of knowledge was in four stages, namely adhiti (study), bodhah
- (understanding), aachaarana- (adoption or practical use), and
praacharana- (propagation). Pracharana, in this context, does not mean
propaganda, as that word is ordinarily understood. It means, giving
knowledge to person or persons tested and found fit to receive
instruction. Propaganda is pressed into service mostly when the result
aimed at is reaching a large number, for statistical purposes. In
proselytisation, for example, the emphasis is on the number of converts,
and not on the fact that conversion was secured only after those who
were converted had understood, believed, and accepted particular tenets
preached to them. In this process the truth of what we wish to propagate
may be lost sight of. Our ancients were particular that truth and right
understanding should be preserved. So, they were against propaganda in
respect of both religion and knowledge. They believed that the seed of
knowledge should germinate only in proper soil, in order that the fruit
that it will yield later may be good and not forbidden fruit. Therefore,
they laid down that the person receiving knowledge must be proved pakvi,
fit to receive it and benefit from it. This was specially necessary in
the case of mantras, whose literal meaning was "protects by repetition"
mananaat traayate. As physical exercises strengthen the muscles by
constant practice, mantraas strengthen the internal nerves by constant
repetition. In the process, the mind is cleansed, so that the residence
of God within us becomes pure. Mantraas can be studied and repeated
beneficially and preserved only by those who are found fit for such
study by their conduct and daily anushtaanaas.

When a person, by his study, observation and experience, comes to
certain conclusions on problems which he considers beneficial to all, it
is desirable that he should either record those conclusions in writing,
or communicate them to those who are capable of appreciating the same,
so that the benefit of his opinion may not be lost to the world. It may
be that some of his views are not accepted immediately, or acted upon.
But if it benefits even one kindred soul, it will be enough. Bhavabhuti
dealing with this points sasys:

> Ye naama kechidiha nah prathayanti avajnaam
> Jaananti te kimapi taan pratinaisha yatnah
> Utpatsyatesti mama kopi samaana dharmaa
> Kaalohyayam niravadhih vipulaa cha prithvee

The substance of this verse is: "Time is eternal and the world is wide.
Some where or at some time a kindred soul may be born who will
appreciate what I have written, even though, for the moment, some may
deride it as useless".

I was reminded of this verse when I read Mr. Hilton Brown's article in
The Hindu wherein he has given the answer to the question, "why do I
live in India". Here is a foreigner who is appreciative of the Hindu
dharmic ideals and practices and finds in this country a peace and
satisfaction which he could not obtain elsewhere.

We must realise the basic principles expounded by our saastraas and
model our lives accordingly. The only lasting thing is our endeavor for
the elevation of soul. Realising this, let us conduct ourselves in the
proper manner. 

