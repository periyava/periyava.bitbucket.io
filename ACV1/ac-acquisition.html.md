Title:Acquisition Of Jnana (Knowledge)

 
 

### Acquisition of Jnana

All of us should strive to acquire Jnana. It is only then that we shall
be able to endure any kind of suffering. No man can escape suffering in
some form or other. Each of us has his or her share of suffering. We may
think that a wealthy person, or a highly placed in life, is free from
cares and anxieties, and, so thinking may covet that wealth or that
status in the belief that we can thereby get rid of our worries. But if
you ask those persons, they will unburden to you their tale of woes. In
fact, every man thinks that his suffering is the greatest, even as he
thinks that he is the most handsome or the most wise. No person dares to
express the latter two feelings openly; but each person thinks that his
sufferings are greater than the sufferings of others and likes to parade
them with a view to eliciting sympathy from others. In a sense,
suffering seems to be our birth-right. Suffering is the fruit of our
actions in previous births. So when we came into the world in the
present birth, we came with the seeds of suffering deeply implanted into
our being. There is no escaping from suffering.

But it is in us to blunt the edge of suffering. An idiot or a lunatic, a
Jada an Unmatta, does not "suffer" as we do. He becomes impervious to
suffering. But when this man is cured of his idiocy or lunacy, as the
case may be, and he is normal like us, he becomes aware of suffering and
begins to suffer as we do. Sleep is the soothing balm for all suffering.
We are oblivious to suffering in dreamless sleep or Sushupti. The
consciousness of suffering in waking life is negated in sleep. But we
relapse into this consciousness when we wake up from sleep. The Jnani
"sleeps to suffering" even when he is awake. It is not that he does not
suffer in body ; but it is that he does not suffer in mind. A heavy log
of wood is not easily lifted or shifted ; it requires a number of hands
to do so. If the same log is immersed in water, it becomes light and
even a child can move it without effort. Similarly, if we learn to
immerse our load of suffering in the water of Jnana, it will become
extremely light and we can make light of our suffering.

What is this Jnana that can lighten our suffering? It is knowing a thing
as it really is. That is the quest of all scientists, namely, to arrive
at the core of the truth of things. And we know that a scientist,
engrossed in his research, loses himself in his pursuit and is
undisturbed by any difficulty or distress. The pursuit of his research
and the joy resulting from the knowledge he thereby acquires, far out
weigh his personal suffering, which becomes very nearly non-existent to
him.

We seek a Vaidya or a Mantravaadi to cure our ills. But whatever relief
either can give will only be temporary. The remedy they prescribe will
not drive out suffering from our system root and branch. The Jnani,
however, is able to get our sufferings, because he develops a sense of
imperviousness to it. Time is a great healer. Thirty years hence, our
present woes, viewed in retrospect, will appear insignificant. We are
also not afflicted by sufferings of people in a distant place, as we are
by the sufferings of people close to us. In the face of present and
proximate sufferings also we must develop such a detachment. When a
person who has acquired such a detachment is commiserated with for any
loss or bereavement he has suffered, his reaction to the offer of
sympathy will be : " It is not anything of much consequence. It came of
its own accord and it went".

How much greater will be sense of equanimity in the face of suffering
when absolute Jnana dawns in the mind? To a Jnani there is no
distinction such as friend or foe. He looks on all as the Paramatma. He
allows nothing to irritate him. He detaches himself from his
environment. He is not afflicted by sorrow or elated by joy. Such a
sense of indifference and equanimity can come only from the knowledge of
the Ultimate Truth. This knowledge must be acquired gradually by intense
meditation or Tapas, as detailed in the Bhrigu Valli of the Taittiriya
Upanishad. Asking the question what is the purpose and purport of life
(kim samsare saaram), Sri Sankara Bhagavatpada answers, in his
Prashnotharamaalika, that it is intense meditation on this question
itself (Bahavopi Vichintyamaanam Idam Eva).

The Jnana that ensues from such meditation alone will teach us to make
light of our own sufferings and also prompt us to go to help of others
in distress, as a matter of duty. Engaging oneself in the acts of public
benevolence and devotion to God produce Chitta Suddhi, cleansing of
heart, so necessary for meditation and Jnana. Such service is not for
show or fame; but for chastening one's own mind. In fact, one ought not
to expect gratitude for the service one renders. The ingratitude of the
other person is a test of one's purity of motive and constancy of
service. Rarely does the beneficiary feel benefited by the help rendered
to him. By such service one does not help the other man so much as one
helps oneself to have Chitta Suddhi.

A true Jnani creates an atmosphere of detachment and holiness around him
and draws innumerable people towards him. Such great Jnanis have arisen
in the world, from time to time, no matter whatever religion they
professed. All this prophets and saints proclaimed the same Truth, each
in his own way, and if they happened to come back to life now and meet
together, there will be perfect unity in their messages. It is the
followers that have put into their mouths more than what they said and
wrangle with others, freezing the original teachings, mangled in their
hands into institutional forms, which foster narrowness and bigotry.

The test of a Jnani is whether all troubles and tribulations of life
appear light to him. This attitude of the Jnani is the sure solvent for
all our ills. To that end we should all strive, doing good deeds and
entertaining devotion to God both of which will be futile unless
oriented to that goal.

**December 7, 1957.**


