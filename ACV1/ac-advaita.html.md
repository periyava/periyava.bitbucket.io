Title:Advaita Vedanta

 
 

### ADVAITA VEDANTA

The school of thought or sidhaanta expounded by Sri Adi Sankara, is
known as Advaita. Greater thinkers who lived before the time of Acharya
have also dealt with it. Wise men who came after Shri Acharya have also
written profusely about Advaita pouring into their writings their own
experience (Swaanubhava), of the Advaitic truth. There are such works
not only in English, but also in Tamil, Kannada, Telugu, Marathi and
Hindi. Some of them are original works of Advaita. Persons belonging to
other schools of Hindu religious thought and persons professing other
religions have also written on Advaita, out of the abundance of their
rich spiritual experience. Some of the names that come to mind are
Tattvaraya Swami, A Madhava, Mastan Saheb, a Muslim and Vedanayagam
Pillai, a Christian. In recent times we have the example of the late
A.V. Gopalacharya, who has written a number of treatises and essays on
Advaita.

It is worthy of note that whatever their mutual differences may be, all
thinkers belonging to schools other than Advaita, are one in their
attack on Sri Adi Sankara's views. This should be regarded as a tribute
paid by them to Sri Sankara Bhagavatpada. Each of them singled out
Advaita, as expounded by Sri Acharya, as the only system worthy of
taking notice of for the purpose of criticising. According to Advaita,
the ultimate bliss is the experience of non-difference between the
Jivatma and the Paramatma. Acharyas of other schools of thought would
wish to have at least a tract of distinction between the two so that the
Jivatma, standing apart, may be able to enjoy the realisation of the
Paramatma. Thus the difference between the several systems of Hindu
religious thought is slight, as all are agreed upon the ultimate
realisation of the Supreme. But when it comes to a question of
expounding each system, this difference got magnified to the point of
violent opposition. And yet we find that in their ultimate reaches, all
of them speak the language of Advaita. This shows that the expansive
heart of Sri Adi Sankara accommodated all views of the ultimate reality
and all approaches to it. Though other systems quarrel with Advaita,
Advaita has no quarrel with any.

The catholicity of Advaita is also evident from the fact that pronounced
Advaitins like Vachaspati Misra, who lived about one thousand years ago,
Vidyaranya and Appayya Dikshitar wrote encyclopedic works on other
systems with the fidelity of exposition rarely equaled and much less
excelled by the protagonists of those systems themselves. Appayya
Dikshitar says that as God's grace is required to reach the Ultimate
Reality, and as that grace can be obtained only through Bhakti, he was
expounding the other systems which promoted this Isvara-bhakti.

According to Shri Adi Sankara, no school of thought is foreign to
Advaita. In the scheme of the path to realise Adviataanubhava, every
system contributes an essential step and so Shri Sankara used the truths
of each of them and pressed them into his service. By its very name,
Advaita negates duality and dissension and comprehends every warring
sect and system into its all-embracing unit. In fact, the survival of
Hinduism is itself due to this Advaitic temper, which sees no
distinction between Saivam, Vaishnavam and other denominations. Shri Adi
Sankara underlined the essential unity of all Sampradaayas and sects and
saved Hinduism from disruption. All denominations have the common Vedic
basis. By bringing to our minds all the great Acharyas, we can acquire
that peaceful frame of mind and develop that catholic temper and
universal accommodation characteristic of Shri Adi Sankara and of the
Advaita Vedanta he expounded, which will enable us to live in peace and
amity, so essential for securing universal welfare. 

