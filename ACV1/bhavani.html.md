Title:Devotion to Bhavani

**DEVOTION TO BHAVANI**

Singing the praise of Ambika, Bhagavatpada says in Soundarya Lahari that
so great is the mercy of the Mother that the moment the Bhakta (devotee)
began his prayer with the words, Bhavaani tvam, Ambika did not even wait
till he completed his prayer, but conferred on him saayujya, viz., the
merger of the soul with the Mother.

***“Bhavaani tvam daase mayi vitara drishtim sakarunaam Iti stotum
vaanchan kathayati bhavani tvamitiyah; Tadaiva tvam tasmai disasi
nija-saayujya-padaveem Mukunda-brahmendra sphuta-makuta neeraajita
padaam”.***

When the devotee began saying Bhavaani tvam, he was only addressing the
Divine Mother by calling Her Bhavaani, and invoking Her grace, though
the expression can also be interpreted as meaning, “May I become You”.
It is this merger with the Supreme that the Mother granted as soon as
She heard the words “Bhavaani tvam”. The significance of this verse is
that one imbued with true devotion gets things unasked.

The saayujya the devotee attains by Her grace is the condition of
supreme saanti, like that which rivers attain when they merge in the
ocean. The same condition of peace and bliss is reached by the bhakta
who starting from dvaita bhaava (feeling of duality of himself and God)
reaches saayujya or oneness with Isvara through Her grace.


