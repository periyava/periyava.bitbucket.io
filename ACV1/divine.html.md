Title:Divine Mother's Kataksha

DIVINE MOTHER’S KATAKSHA

***“Drsaa draagheeyasyaa dara-dalita-neelotpala-ruchaa daveeyaamsam
deenam snapaya krpayaa maamapisive; Anena-ayam dhanyo bhavanti na cha te
haanir-iyataa Vane vaa harmye vaa samakara nipato himakarah”.***

This verse is from Soundarya Lahari, composed by Sri Sankara
Bhagavatpada. Addressing the Divine Mother, Sive, Sri Acharya requests
Her to bathe even him, who is helpless and standing at a great distance,
with the far-reaching glance of Her soothing blue eyes, beautiful as a
half-blossomed blue lily. By this act, the Acharya says, the Mother
stands to lose nothing while he, a deena, will be blessed and enabled to
achieve the goal of life. The soothing cool moonlight falls equally on a
forest and a beautiful mansion. By this verse, Sri Adi Sankara seeks to
convey the idea that the range of the benevolent look or kataaksha of
the Divine Mother is long enough to embrace everything and every one in
this wide world.

The Divine Mother is part of Isvara. Sri Adi Sankara, an incarnation of
Isvara, is the embodiment of the Divine Mother also. Yet for the purpose
of instilling bhakti in the minds of the people, he humbles himslef by
describing himself as deena, or helpless, and considers himself as
standing in the last place in the queue of people waiting to receive the
grace of the Mother. By invoking Her to bless “even him” (maamapi), he
suggests, by inference, the existence of persons more deserving than
himself to receive Her grace. On the other hand, he describes the glance
of the Mother as reaching the far ends of the Universe and embracing
everything. It is in such a spirit of humility and intense devotion that
Sri Adi Sankara has given to us a rich devotional treasure in the form
of Soundarya Lahari and it is up to us to benefit from it.

November 6, 1957


