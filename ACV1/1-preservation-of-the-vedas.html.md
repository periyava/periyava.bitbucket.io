Title:Preservation of the Vedas

                                               
### Preservation of the Vedas

 

All of us take care to keep our bodies and our clothes clean. But do we
bestow any attention on our inner or mental cleanliness? Inner impurity
is the result of desire, anger, and fear. It is common knowledge that
when one is in the presence of one’s mother, one keeps all evil thoughts
under control. Similarly, in the presence of the Divine Mother, we can
control our evil thoughts. We can cleanse our hearts only by the
*Dhyana-thirtha* (holy water of meditation) of the Divine Mother. When
the heart is so cleansed, it will learn to distinguish the real from the
unreal, which will result in the end of births. A day spent without a
conscious attempt to clean one’s heart, is a day wasted. Impurity of
cloth or body will lead to diseases which will last only for one
life-time. But impurity of heart will lead to diseases which will
afflict the soul for several births.

 

God or Paramatma is only one, and we worship that God as Father, Mother
or Teacher of the Universe. The Vedic religion, which is popularly known
as Hindu religion, emphasises this fact. God in the form of Divine
Mother is a personification of kindness and love and he who worships at
Her divine feet will secure mental peace quickly. Desires only increase
by fulfilment. Desires can be overcome by *saanti* and mental
discipline. Let us surrender ourselves at the holy feet of the Divine
Mother and purify ourselves with her *Dhyana-thirtha*, and thus free
ourselves from desires, diseases and births.

 

There are two main sects among Christians. But the name of the God and
the Holy Book of the Christian religion are common to both. The same is
the case with the Muslims. So far as the Hindus are concerned, there are
apparently two Gods and two Holy Books, according to whether one is a
Saivite or a Vaishnavite – the Tirumarai and the Prabandham. But the
basis for both Saivism and Vaishnavism is the Vedas, and according to
the Vedas, there is only one God, the God about Whom the Vedas sing. If
we had been classified as Saivites, Vaishnavites, and so on, the whole
country would have been Balkanised. We should, therefore, bear in mind
the fact the the Vedas form the basis for our religion and that there is
only one God. Failure to realise this fact will only lead to the
weakening, and finally the disintegration, of Hindu society.

 

This takes us to the question of preserving the Vedas in their pristine
purity. The Vedas are not preserved in writing and the Tamil term
*marai* (hidden) for the Vedas is very appropriate. The Vedas are like
the roots of a tree. The different sects are like its flowers and
fruits, all deriving their sustenance from the roots. Fortunately, we
have the good tradition of the Vedas and the Vedangas being handed down
from generation to generation by word of mouth, and happily for the
entire world, the Vedas have been preserved in their pristine purity,
especially in the South.

 

The importance of Sanskrit is due to the fact that it is the language of
the Vedas. There is evidence to prove the influence of Sanskrit in
Far-Eastern countries like Indonesia and even in places like Persia. It
once occupied the place of an international language. The Vedas must be
preserved in the Sanskrit language and not in translation, because the
spirit will get diluted in the process of translation. Though there may
be translations, a reference to the original will become necessary, when
difficulty arises in interpretation. We can trace the basis for all
religions to the Vedas. For the preservation of Vedas, it is necessary
that some people devote their entire time for Vedic study. That is how
the Vedas were preserved in the past and were handed down to succeeding
generations by oral transmission. A community will cease to exist the
moment it loses sight of its purpose in society. The purpose of the
Brahmin community is to learn, preserve and hand over to posterity, the
Vedas and the Vedangas

 

**October 5, 1957**
