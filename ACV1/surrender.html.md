
**SURRENDER TO DIVINE MOTHER**

> ***“Sampatkaraani sakalendriya-nandanaani
> Saamraajya-daana-vibhavaani saroruhaakshi
> Tvad-vandanaani duritoddharano-dyataani
> Maameva maatah anisam kalayantu maanye”***

O! Mother, who has eyes as beautiful as lotus flowers and who Is worthy
of worship, let the obeisance offered to you, obeisance Capable of
bestowing property, bring blissful joy to the Idriyas, Having the power
to gift an empire, and remove sins and purify, Always remain with me.

This sloka is from Sri Sankara Bhagvatoada’s Kanakadhaaraastava. As a
Brahmachari Sri Sankara recited this and caused a shower of gold for the
benefit of a poor housewife who had nothing to offer as bhiksha to him
except a solitary aamalaka fruit (Nellikkani). The significant portion
of the verse is where Sri Adi Sankara prays to the Divine Mother that
Vandana (obeisance) offered to Her with the purpose of rooting out
durita (sin) – duritoddharana - should not leave him, but remain with
him alone always (maameva anisam kalayantu). This Vandanam is alone is
my property – the Mother is also the giver of wealth – and should remain
with me, he says. The import of this sentiment is that the Mother
(maatah – hence Taayaar for Ambika in Tamil) in her mercy should help
him to hold fast to the Vandanam to Her. The only way to get oneself
cleansed of one’s sins is to penitently prostrate at the feet of the
Divine Mother. Let us, therefore, surrender ourselves at the feet of the
Mother and find peace and happiness.


