Title:Nature Of The Vedic Religion

 
 

### Nature of the Vedic Religion

We should all strive to cultivate lofty and noble sentiments, and ,
eschewing all bad and selfish thoughts, live in a spirit of devotion to
God and love for fellowmen. Human stature increases in proportion to the
nobility of human thought and deed. The spirit of selfless service, the
readiness to sacrifice, devotion to God , and love for and goodwill
towards all, and hatred for none, are the outcome of highly developed
mind, and go by the name of culture. Culture is known as KALAA in
Sanskrit, and arts like music, painting, etc., are regarded as the
outward expression of this high culture.

It is interesting to note the verbal affinity that exists between the
works KALAA, CULTURE, KAL(the Tamil word for learn ), KALAASAALA, and
college. A man of culture is kin with the whole world. He is the friend
of all and enemy of none. For him the three worlds are his home land (
SVADESO BHUVANATRAYAM). The culture of people is judged by the soundness
of the heart of the people taken as a whole, though there may be
individuals with defects and deficiencies.

The touchstone of the culture of a nation is the inspired sayings of its
immortal poets ( MAHA KAVI), whose poems have stood the test of time.
These immortal poems flow from the fullness of their heart and are the
expressions of the noble culture which they represent and in which they
are steeped. These great poets have no private axe to grind. Having no
pet theories or sectarian SIDHAANTAAS to bolster up, they have no need
to import specious arguments in their poetry. They give expression to
truth ; their insight into truth gives them the courage of utterance.
Their authority is accepted to prove the culture of the people in whose
midst they flowered. Homer and Shakespeare are two among such great
poets in the West, and in our country Kalidasa and Baana are great poets
without a peer. It is said that the ring finger came to be called
ANAAMIKA in Sanskrit, because a person who wanted to take a count how
great poets, counted first Kalidasa on his little finger, but could not
think of any fit person to count on the next(ring) finger. So that
finger came to be known as nameless or ANAAMIKA. As regards the
greatness of Baana, there is a saying that other poets used the crumbs
that were left over in Baana's plate(BAANOCCHISHTAM JAGAT SARVAM). Thus,
these poets have come to be regarded as great masters. Their verdict is
accepted as authority, not only in matters pertaining to culture, but in
religious matters also.

In the context of our daily life, we are frequently called upon to
determine the nature of our duties, or DHARMA. The question arises, what
is our Dharma and from what authority is it derived? Ordinarily, the
enactments of the legislature, i.e. the laws of the state, regulate our
public conduct. These laws derived their sanction from the constitution
adopted by the representatives of the people. The laws are also enacted
by the elected representatives of the people. It does not require much
argument to show that the voters are of various grades of intellectual
and moral calibre, and that not all the representatives they elect are
the best that could be found. Such a state of affairs is inevitable in
this imperfect world. Some of the laws may not also be perfect from the
moral point or view. That is why occasionally we hear judges remarking
that they decided a point according to law, though they are not
convinced of its moral correctness.

In our day-to-day personal and moral conduct, signified by the
expression DHARMA, our religion has declared that we should be guided by
the ordinance of the Vedas. It is declared that Veda is the source of
all DHARMA( Vedokhilo dharma moolam). To illustrate the vastness of
Veda, there is a story that what Sage Bhardwaja was able to learn was
compared to a handful of dust taken from mountain the mountain
representing the Vedas. If a doubt arises, which cannot be solved with
reference to the Vedas, we are enjoined to seek guidance from Smritis.
It is a mistake to regard the authors of the SMRITIS , like Manu,
Yajnavalkya and Parasara, as law-givers. SMRITIS are merely AIDE MEMOIRE
or short notes, meant to indicate what are contained in the Vedas. The
authors of the SMRITIS did not write anything new, apart from what is
contained in the SRUTI or the Vedas. There is authority of Kalidasa to
this proposition. Describing the manner in which SUTEEKSHNA followed,
for a short distance, her husband , King Dileepa, when he took Nandini
out to graze every morning, Kalidasa states that she followed the
footsteps of Nandini, like SMRITI following the meaning (footsteps) of
SRUTI(Sruterivaartham smritiranvaghacchat,). Kalidasa has unambiguously
established that the way as Sutekshna following Nandini only for a short
distance, the SMRITIS only briefly indicate what SRUTI contain.

If we are unable to get the necessary guidance to clear out doubt either
from the SRUTI or from the SMRITIS, we are asked to be guided by the
conduct of those who know and follow SMRITIS. When this guidance is not
available, we are asked to model our conduct on the action of good
people who have conquered desires and ego, and are pure in heart. When
even this source of guidance fails. We have to abide by the dictates of
our conscience. That is how Dushyanta reconciles himself to the love
which sprang up in his breast at the sight of Sankuntala in Sage Kanva's
aasrama. Being aware that it is wrong for a KSHATRIYA to fall in love
with daughter of a sage, he concludes that having entertained no evil
thought before, his conscience could not have misled him into falling in
love with a wrong person. PRAMAANAMANTAHKARANA-PRAVRTTYAYAH, says
Kalidas. It is to be noted that Sri Vedanta Desika in his
RAHASYATRAYASAARAM has quoted this KAVI-VAKYA, this authority of
Kalidas, in support of a proposition enunciated by him. Kumarila Bhatta
has also cited Kalidasa's authority in his work.

In these days it is fashion to give preference to conscience and
relegate all other Sastraic guidances to a secondary place, or, as is
often done, to condemn them a antiquated , meaningless and irrational.
But according to our SASTRAS, the appeal to the conscience must come as
the last resort, when all other guidances like SRUTI, SMRITI, etc., are
not available. The modern view is at variance with classical view of the
authorities on dharma. The ancient view has stood the test of time and
makes for enduring and eternal sanction in respect of ethical conduct.
This view has been voiced in the utterances of MAHAKAVIS like Kalidasa,
whose voice is Truth, which is glory and the prerogative of great poets.

Foreign critics of our Vedic religion fling at us the cheap gibe, "What
a host of gods and goddesses you worship!" This charge of polytheism
leveled against our religion is entirely wrong and is born out of
ignorance of the fundamental teachings of the Vedas. This what Baana
says on this subject :

>     " Rajo jushe janmaani satva-vrrtaye
>     Sttitau prajaanam pralaye tamsprse
>     Ajaya sarga stthiti nasa  hetave 
>      Trayeemayaya trigunaatmane namah."

In this verse Baana says that the One God appears in the three forms of
Brahma, Vishnu, Siva, for a three-fold purpose, namely creation,
protection and dissolution, which functions are determined respectively
by the qualities or GUNAAS of RAJAS(H), SATVA and TAMAS. That One is the
unborn (Aja) and is the cause of these triple process. He is trayeemaya,
compound of the three aforesaid qualities. He is trayeemaya also in the
sense that He is claimed by the trayi or the Vedas . Kalidasa expresses
more or less the same idea when he says :

> " Ekaiva  moortirbibhide  tridhaasaa
> Saamaanyameshaam pratha maavaratvam;
> Vishnor-harastasya harh kadaachit,
> Vedhastayostaaavapi dhaaturaadyau."

One moorti (manifestation in the form ) appears as three, and there is
no question of any One of the Three being superior or inferior to the
other Two , says Kalidasa. If Brahma, Vishnu, Siva are One in essence ,
the, by the same token, all the Gods of the Hindu pantheon are also one
in the ultimate analysis. Then why this wrangling that one god is
superior to the rest? Some assert that the deity they worship is alone
the highest . To a man standing under the arch at one end of a bridge ,
all the other arches will appear smaller than the one under which he is
standing. But we are aware that all the arches of a bridge are of the
same span. Similarly, to the votary of a particular deity, all other
deities will appear inferior on account of his attachment to the deity
of his choice. But the truth is that all deities are manifestations, in
particular ways , of one God.

God is the final of all the things of the world. If we take the example
of a tree, we will find that it is soil an d water that help the seed
grow into a mighty tree. The source from which the tree came into
existence from a seed, is the soil and water. The tree is sustained
during its existence by the same soil and water. When the tree dies, it
resolves itself into the soil and water from which it sprang. The
essence or truth of the tree is the soil. It is the same for all
material things like trees which constitute the world. This principle of
an identical source is applicable in the case of other forms of creation
, including animals endowed with intelligence. As there is a "universal
soil " at the back of "individual" soil from the which a tree springs ,
by which it lives and into which it disappears, so too there must be
Superior Intelligence (Perarivu) of which our intelligence are but
minute fractions. That Superior Intelligence or Chit is God. He is
Ananda or Bliss. He is the one existent or Sat. He is responsible for
creation in conjunction with Rajo guna, for preservation motivated by
Satva guna and for destruction under the impact of Tamo guna. Thus God
is trigunaatma. One appearing as Three. Ekaiva moortih bibhide
tridhasaa.

Parabrahmam, which is without attributes(Nirguna) which is pure or
suddha satva, becomes the personal God or Isvara. Isvara has to perform
these three functions of creation, protection and dissolution. But the
Suddha Satva Isvara is static. He has to become dynamic to perform the
act of creation. Rajo guna supplies the energy to act, and so, in
conjunction with it, the one primal God becomes Brahma, The Creator.
What is created must be maintained and made to grow and flourish. That
is accomplished by Iswara assuming Satva guna. In that aspect, He is
Vishnu, whose consort is Lakshmi, the embodiment and bestover of
prosperity. To bring about death, or the end of things created
association with Tamo guna becomes necessary. That aspect of Isvara is
Siva. It is to be remembered that the Samhaara kaarya (dissolution)
associated with Siva does not signify cruelty on His part. It only
betokens His mercy for the created, by which He gives rest to the
ignorant souls, who have a balance of unrequited karma, for the duration
of the Pralaya, before they are pushed in to the next cycle of the Birth
to work out their residual karmaas. These three attributes, Rajoguna,
Satvaguna, and Tamoguna , do not really belong to Iswara. He is Suddha
Satva Swaroopa. He gets mixed with each of the three gunaas for definite
purposes, and appears in different forms as a result. Only His
appearances are different; not His essence.

This characterisation of Brahmaa, Vishnu and Siva as denoting Rajoguna,
Satvaguna, and Tamoguna respectively, is not absolute either. Vishnu,
who is considered as symbolising Satvaguna, has, on occasions, taken
upon Him self Tamoguna, standing for destruction, as His Avataar as
Narasimhaa. In the Rama Avataara, when He fought Khara Dooshana, Kumbha
Karna and Ravana, and also when he threatened to dry up the ocean, He
assumed Tamoguna. Vaalmiki very appropriately describes this aspect when
He says that Rama took up on him self intense anger, Kopamaahaarayan
Theevram. Anger is the effect of Tamoguna. Per contra, Siva, who nature
is said to be Tamoguna, being the manifestation responsible for
destruction, likewise assumes Satvaguna in His aspect as Nataraja and as
Dakshina Moorti.

Thus these forms of God are not distinct and different. They are three
manifestations of the same Divinity assuming aspects for different
purposes, and according to the predilections and tastes of the
worshipers. It is wrong to speak of gradations of excellence among them
or to say they are diverse and different. The forms may appear
different, the names may be different, but the truth is one. It is One
that becomes Three, and then Thirty Three, and then Thirty Three Crores,
according to the numberless varieties of functions of Divinity. This is
the basic fact declared by the Mahaakavis and their words must determine
us in our devotion and religious practices.

January 4, 1958. 

