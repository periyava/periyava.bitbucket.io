Title:Many Paths To Same Goal

 
 

### MANY PATHS TO SAME GOAL

The dancing Nataraja and the reclining Rangaraja are but dual
expressions of the one Supreme. Different schools of philosophy have
come into existence to satisfy the needs of varying human temperaments,
tastes and aspirations and any path, if consistently pursued, will lead
to the same goal.

In most of our temples, the principal deity is installed to face east,
though in a few temples we have the deity facing west also. In the
latter case, the principal gopuram (tower) will be on the eastern side.
But in Chidambaram and Srirangam, the deities face south, as if
proclaiming to the devotees that they are there to protect them from the
threat coming from the south, namely, mortality, as the God of Death,
Yama, hails from that direction. As Lords of the entire created world,
both are called "Raja", and each holds His court in a ranga (stage), the
Lord of Chidambaram dancing in joy with uplifted leg and the Lord of
Srirangam stretching himself at ease in the repose of yoga nidra.
Dakshinamurthi, another aspect of Siva, is also found facing south.
Nataraja stands for aananda (bliss) in excelsis, which expresses itself
in the dynamic rhythm of ecstatic dance, His matted locks stretching out
stiff as He whirls round with his kunchita paada. Dakshinamurti
personifies subdued aananda and He is depicted as being seated in silent
serenity in static pose, with one crossed leg resting on the other and
his locks gliding on his shoulders.

The Maheswara Sootras peal forth from Nataraja's dhakka, (Udukku), as He
beats it to keep time with His dance, and constitutes the basic
alphabets of every tongue spoken in the world. The same sounds or sabdas
are recorded in the pages of the book which Dakshinamurti is holding in
His left hand. Aananda mudra is expressed by the right hand of Nataraja,
while Chin mudra is expressed by Dakshinamurti. We stand and gaze in
wonder with eyes wide open at Nataraja's dance, but we sit down to
meditate with indrawn eyes in front of Dakshinamurti. To the former we
go for darsana, for feasting our eyes with the supreme majesty of that
manifestation, to the latter we go for japa or meditation, because He is
the embodiment of the fullness of peace and bliss that comes as a result
of jnana. Ranganatha has adopted the nidra mudra - the sign of sleep.
All these three, the Aananda murti (Nataraja), the Dhyaanamurti
(Dakshinamurti) and the Yoganidraamurti (Ranganatha) face south to
protect mankind from the fear of death.

The question that is likely to arise is "why should there be three
deities? Is not one enough?" The answer is given by Pushpadanta in his
Sivamahimna Stotra.

> Trayee saankhyam yogah pasupati-matam vaishnavamiti
> Prabhinne Prasthaane param-idam adah patthyamiticha;
> Rucheenaam vaichitryaat rjukutila naanaa pattha jushaam,
> Nrnaam-eko gamyas-tvamasi payasaam arnava iva.

The variety of schools, namely, Vedas, Saankhya, Yoga, Paasupata, and
Vaishnava came to be formulated to satisfy the varying tastes of men.
Though their directions may appear to point differently, yet, as one
pursues any school with the constantly ordained it, after a shorter or
longer journey, as the case may be, one will ultimately reach the
Supreme, which is Omnipresent, even as all rivers flowing in different
directions reach the ocean, which appears at land's end everywhere and
envelops the globe in all directions. Like the ocean, the Supreme
envelops all - sarvam aavrtya tishthati.. To whatever school one may
belong, one ought not to linger or stop on the way. If a person adheres
to the chosen path without faltering, God will dower each votary,
whatever his predilection, with constancy of faith to pursue his path
with devotion. All of you are familiar with the scene at a railway
station, as soon as a train arrives and the passengers emerge out of the
platform. A passenger will be stormed by drivers of a variety of
conveyances, each trying to snatch his baggage in order to attract him
to his vehicle. In whichever conveyance he ultimately decides to travel,
his destination is his home. Similarly the protagonist of each school of
religious thought try to attract the seeker after truth by saying that
their school is the easiest and surest way to realise the truth. When it
is recognised that all paths lead to the same goal, there is no
necessity to change the path one is already following. There is also no
room for hatred towards a person following a different path. The temple,
the God installed therein, and the form of worship, all these three may
differ for different people, due to difference in taste. But what is
required of one is to persist in the path one is following.

When the mind becomes ripe with the true knowledge of Paramatma, the
soul gets liberated from the bonds of birth and death. This liberation
is called Moksha or Salvation. The Trayambaka mantra epitomises the
special kind of Moksha, which accrues by the grace of Trayambaka, the
three-eyed Siva. The Mantra conveys the meaning that one is released
from mortality by the grace of Siva in the same way as the cucumber
fruit gets separated from its stalk, that is, automatically separated
without even the cucumber being aware of its liberation from the creeper
to which it has been all along lying attached. Every fruit, when fully
ripe, is sweet, though it may have been bitter or sour when unripe.
Similarly, when the soul becomes ripe through devotion, it is filled
with the sweetness and joy that comes from Jnana. All fruits fall down
from the branches on top, at the roots below, signifying that the root
is their source, sustenance and ultimate sanctuary. The ripe soul,
however, is the fruit of the tree of Samsaara, worldly bondage, whose
roots are on top, Oordhva moolam and whose branches grow down below
(Atha shakham). So the passage of the liberated soul is upward, Oordhva
gati, and not downward or Adho gati. Strictly speaking, there is no gati
or going, for the soul. It is released at the very place where it
existed. That is why the example of cucumber fruit is given. This fruit
does not fall down but gets itself detached from the stalk, or rather,
the stalk gets itself detached, even without the fruit knowing it.
Similarly the liberated one does not give up the world; the world gives
him up. Remembering that this life has been vouchsafed to us to get rid
of future births and deaths, let us pray to the God of our heart, to
obtain His grace to qualify for this kind of liberation of the soul,
"cucumber mukti".

November 21, 1957. 

