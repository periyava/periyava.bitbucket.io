Title:Importance Of Bhakti (Devotion)

 
 

### Importance of Bhakti

Advaita, Visishtadvaita, and Dvaita are one in the emphasis on Bhakti to
obtain God's grace. The fact that man alone, among the creatures of the
world, grows vertically, where as the other creatures grow horizontally,
indicates that he should also strive to grow taller in spiritual
stature. Such an eminence in stature comes from Jnana which alone gives
abiding peace or saanti. Man undergoes troubles and pains in a greater
measure than other animals; but that is compensated for by this capacity
to acquire Jnana, which makes for the realisation of Truth and the
experiencing of Aananda or Supreme Saanti. Jnana itself begets Aananda
and Jnanaananda is Parama mangalam. Isvara is of the nature of this
Jnanaananda. He is the Paramataman in whom all auspicious qualities are
fully affirmed in a superlative measure. Even as the ocean is the
repository of all waters, Isvara is fullness, the All. There is no other
to him. He is the All with out a second. That is Advaita. Idam Sarvam
Purusha Eva, all this is the Paramatman, says the Sruti. Sri Sankara
expounds this truth with Yukti(logic) and Anubhava(experience).

But mere intellectual comprohension of it is not enough. It must be
realised as a fact in one's own experience. For such realisation grace
of God is a pre-requisite. Isvaraanugrahaadeva Pumsaam Advaita Vaasana.
We begin with a feeling of distinctness from God. The predicament of
worship is one of duality of Deity and Devotee. But even then the
devotee does not feel that God is external to him and to the Universe;
He has the consciousness that God is imminent in himself and in every
particle of the world, in-dwelling every where and in everything, how
ever minute. Our duty is worship Him in this way with devotion, and if
we do so, He reveals His true nature to us. Bhagavan says :

BHAKTYAA MAM ABHIJANAATI YAAVAAN YASCHAAMI TATVATAH

The word, BHAKTYAA, meaning through devotion, shows that bhakthi is the
means for the realisation of the truth of God's nature. Advaita,
Visishtadvaita, and Dvaita are one in this emphasis on Bhakti to obtain
God's grace. To whatever school we belong, we should invoke that grace
through Bhakti, leaving it to Him to reavel the truth of His nature. All
Achaaryas have stressed this need for Bhakti.

This devotion must evidence itself in fulfilling God's commands in
observing the duties laid on us in the Vedaas. To say 'I have devotion
to God', and not to act upto His commands is meaningless. Performance of
prescribed duties, Vihitikara-maanushtaana, is the sign of true
devotion. Doing one's Karma, one should dedicate it to God. Obligatory
duties are Nitya and Naimittika. Vaishnavites significantly refer to
them as Aajna Kainkarya. Such performance of Karmas as detailed in the
Vedaas is true Isvara Aaradhana. They not only bring about personal
merit, but also secure the welfare of the whole world. Thus Karma,
Bhakti, and Jnana do constitute the scheme of salvation.

October 16, 1957. 

