Title:Nature of True Bhakti

### Nature of True Bhakti

True *Bhakti* or devotion is that condition of a devotee’s mind when it
is unable to bear even a moment’s separation from the shelter of God,
and when even if is forcibly withdrawn from that shelter, by force of
circumstances, it struggles and rushes back and attaches itself to God,
like a needle to a magnet.

 

> Ankolam nija beeja-santatih
> ayaskaantopalam soochika
> sAdhvee naija vibhum lataa kshitiruham
> sindhuh saridVallabham;
> praapnoteeha yathaa tathaa
> pasupateh paadaaravindadvyam,
> chetovrittir-upetya tishthati
> sadaa saa bhaktirityuchyate.


This verse occurs in Sivaananda Lahari, and in it, Sri Sankara
Bhagavatpada has explained what real *bhakti* is. The relationship
between the devotee and *Isavara* is explained with reference to five
examples. They are: the tree known as *ankolam* and its seeds; the
lodestone and the needle; a chaste woman and her husband; a creeper and
a tree; and a river and the ocean. The ankola tree (*azhinchil* maram in
Tamil) is found in the forest. It is that when its fruit falls to the
ground, the seeds, liberated from the fruit by some compelling force
within, move close to the trunk of the tree, gradually climb up, and get
inseparable attached to the tree. During my travels, I was shown this
tree in a forest. I saw the seeds sticking to the trunk of the tree,
though I was not able to observe the actual movement of the seeds from
the ground to the tree. The example of the seeds which fall away form
the tree struggling back and attaching themselves to the tree, is
denoted by the words, *ankolam nija beeja santatih.*


The next example given is *ayakaantopalam soochika .*Ayaskaanta* means
magnet, *upalam* means stone, and *soochika* means needle. When a needle
is brought near a lodestone, it rushes towards the stone and gets itself
attached to it. Similarly, the mind of a devotee rushes towards God and
finds a heaven there. The next example is that of a *saadhvee*, a
*pativrata* or chaste woman and her husband, is significant. The literal
meaning of *vibhuh* is, one who pervades everywhere. The idea Sri
Sankara wants to convey by using the term *vibhuh* is that a true
*pativrata* has only the thought of her husband uppermost in her mind,
all the time, whatever other objects may be in front of her eyes. She is
so saturated with the thought of her husband that she sees her husband,
whichever way she may turn. So also a *bhakta* see only God in
everything around him.

 

The example of *Lataa* creeper, and *kshitiruha* tree, is next given to
indicate the mind’s frantic efforts to get itself attached to Isvara
like a creeper to a tree. As a creeper grows, its shoots sway hither and
thither, in an attempt to get a hold on something to which they can
attach themselves. The moment the shoots come into contact with a
neighbouring tree, the creeper winds itself around that tree, get itself
attached to the tree. The mind of the devotee is constantly in search of
Isvara, and the moment He is realised, it attaches itself to Him
inseparably.

 

 

The last example is that of the *sindhuh,* river and
*SaridvallabhaOcean*. A river has a small origin on a mountain. In the
intial stage of its course, which can be compared to our own childhood,
the river is noisy, plays about by jumping from one rock to another, and
is resless and so flows fast. Its speed reflects its anxiety to join the
ocean. When nearing the sea, the river becomes calm and placid. This
state can be compared to a woman’s humility, shyness, and serenity in
the presence of her husband. The ocean, being a loving husband, rushes
forward to receive the river in her arms. That is why the river water is
saltish for some distance inland from its mouth. Similarly, the restless
soul finds serenity when it reaches the proximity of God, and finally
gets engulfed in that ocean of Supreme Bliss.

 

Sri Sankara has expounded *advaita tatva* both in the main them of the
verse and in the illustrating similies. Water from the sea evaporates
into cloud and returns to the earth as rain. The rainwater goes back to
the sea as rivers. In that way a circle is completed. The river and the
sea, though apparently two, are in reality one. By the process of
evaporation, the volume increased by the inflow of river waters. In the
same way, everything in this universe is part of God. He is everything
and everything ultimately merges in Him. He is Full always, and His
fullness is in no way affected either by creation or by the merger in
Him of the created beings. The human soul, *jeevatma*, is restless like
a creeper, in search of a support to sustain it, and eager to rejoin its
source, like the river is to rejoin the ocean, its ultimate source. As
the *jeeva* gets to be more and more proximate to God, it obtains saanti
or serenity, like that which the waters of a river attain near the
river’s confluence with the sea. The *bhakta*, who eventually becomes a
*jnani* see only Isvara in everything, even as a *pativarata* thinks
only of her husband and lord. When the should finally finds its haven in
the *Paramaatma*, it unites with the *Paramaatma*, like creeper bugging
a tree, or a needle flying to and getting attached with a magnet. If,
for any reason, the *jeeva* is forcibly detached from Isvara, it becomes
restless, struggles and eventually gets back to Isvara.

 

When our devotion to God is motivated by a desire to secure some earthly
benefit, it ceases to be real *bhakti*; it becomes a barter. But when
our bhakti is for our spiritual elevation, we attain the saanti of the
river when it is near its Lord, the Ocean. The devotee begins his quest
for bliss with devotion to One, who, he thinks is outside him. When the
devotion is selfless, that is, when the quest is a quest of his own real
self, the *dvaita* bhaava (the duality of God and himself) changes into
advaita *bhaava*, the oneness of himself and God. He surrenders himself
absolutely and unreservedly to the Paramaatma, and becomes one with that
Only *paadaaravinda dvayam Chetovrithirupetya tishthati*. We must all
strive to develop the kind of devotion to Isvara indicated in the verse
from Sivaananda Lahari I have quoted in the beginning.

 

**February 8, 1958**
