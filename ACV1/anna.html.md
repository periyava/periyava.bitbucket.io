Title:Mother Annapoorna

**MOTHER ANNAPOORNA**

When Sri Sankaran Bhagavatpada visited the shrine of Sri Annapoorna,
during his stay in the holy city of Kaasi, he composed a hymn in praise
of the Devi in eight verses, known as Annapoornaashtakam. This hymn is
recited with great reverence throughout India. Each one of these verses
ends with the refrain,

“Bikshaam dehi kripaavalambanakaree maataannapoornesvaree.

One of the verses describes the Divine Mother as Aadikshaanta samasta
varnanakaree. The fiftyone letters of the alphabet from a ksha, go by
the name of varna. Varna also means the four castes. Another meaning of
varna is colour. The Divine Mother is is soul of the varna or alphabet.
The sastras which are based on sabda (sound) are the sound forms of
paradevata. Sabda gives rise to forms – visible shapes. It is observed
that when particular musical notes are played near a pond, the resultant
vibrations induce particles of light dust, floating on the water, to
arrange themselves into specific shapes. Thus sabda and roopa, sound and
form, have close affinity. This also accounts for the sanctity of
mantras, which are words and letters combined and arranged in specific
forms. The repetition of a mantra, with devotion, earns for us the grace
of the particular manifestation of God for whom that mantra is
dedicated. The Divine Mother is the soul of all mantras.

The conception of Divinity as the Mother is unique and inspiring. In
human relationship the affection of a mother for her child is
unsurpassed. Similarly, the depth of the Divine Mother’s love for her
devotees is unfathomable. The grace that flows from Her is spontaneous
and irresisble. That is why Sri Annapoorna is depicted as carrying a
vessel containing ksheeraannam (rice mixed with milk) in one hand and a
ladle in the other. She is ready to distribute this food to those who
pray for it. In the abundance of Her mercy, She gives us not only food
that sustains our body, but also jnana that nourishes the soul. When Sri
Adi Sankara prayed to Mother Annapoorna to give him alms, he prayed not
only for himself but for all mankind. We are all members of one family.,
being the children of the Divine Parents, Paravati and Paramesvara. It
is our duty to love, help, and serve one another.

There is a temple dedicated to Sri Annapoorneswari at Cherukunnam in
Kerala. Every devotee who worships at that temple is served with food.
The tradition is that in the night, after every one is fed, a packet of
food is left tied to the branch of a tree, the idea being that even the
thief who prowls about in the night should not go without food.

Let us reverentially pray to Mother Annapoorna by reciting the immortal
Annapoornaashtakam of Sri Sankara Bhagavatpada and earn Her grace for
the welfare of the entire world.

May 16, 1958


