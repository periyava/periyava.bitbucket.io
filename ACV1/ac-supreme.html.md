Title:One Supreme Being

 
 

### One Supreme Being

Some western scholars in their ignorance have dubbed Hindu religion as
polytheistic. The uniqueness of our religion lies in the fact that under
whichever name a devotee worships his Ishtadevata-that manifestation of
God which appeals to him most-he considers Him as all-pervading
Paramatama. In fact, the culmination of all conceptions of the Supreme
Being is in Monism. That is Advaita Vedanta. Isvara, Narayana and
Parasakti are all different aspects of the one Supreme Being. This is
visibly illustrated in the divine forms of Ardhanareeswara and
Sankara-Narayana. Such manifestations of the Divine are installed in
many South Indian temples, such as Ardhanareeswara at Triuchengode,
Sankaranarayana at sankaranarayana Koil in Triunelveli, and Harihara in
Mysore. Siva and Vishnu are also found together in the temple at
Tiruparkadal near Kaveripakkam.

November 10, 1957 

