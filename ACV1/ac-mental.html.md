Title:Mental Discipline

 
 

### MENTAL DISCIPLINE

In the Gita, emphasis is again and again laid on developing that mental
equipoise which is not disturbed either by adversity or by prosperity.
This state can be achieved only by completely surrendering ourselves to
God. Bhagavan makes this clear by directions like Yuktha Aaseeta Mat
Para, Maamekam Saranam Vraja, and Vasudevassarvamiti. An Ayuktha, i.e.,
one who cannot view everything with equal unconcern and consequently
whose mind is easily assailed by desires and passions and who is unable
to do his duty in the spirit of dedication, neither profits from
knowledge nor acquires a spirit of devoted surrender (Bhaavana). Without
Bhaavana he cannot find peace (Saanti) and without Peace, he does not
attain absolute happiness (Sukam). It is this teaching of the Gita that
inspired Saint Thyagaraja to sing Saanthamu lekha Saukhyamu ledhu. If
the mind runs after worldly pleasures, all efforts to find real
happiness will go in vain like ghee poured in a leaky vessel. It follows
that Yoga in the Gita sense is necessary both for acquiring Paroksha
Jnana, knowledge of the Ultimate Reality, and Aparoksha Jnana,
Realisation of that Reality.

It is to be noted that the Samatva or equal feeling in the context of
Yoga does not denote universal equality, but only the capacity to treat
good and bad results with the same feeling. The stress is on the
performance of one's allotted duty. That duty should be performed well,
without thought of reward and with devotion in the heart. While
performing it, the act should be dedicated to God. This is not possible
without the control of sense-organs. The Indriyas should be withdrawn
from the objects which attract them. If they run away unbridle, the mind
will also run away along with them. Such a wavering mind cannot
contemplate the Atman within, by which process alone the ultimate Truth
can be realised. When the mind is tossed by Indriyas, Prajna will go out
of proper cores like a ship tossed by high winds in the ocean. The
distinction made in the Gita between mind and Prajna is to be noted. In
Sanskrit, the mind is known by different terms according to its
functions, like thinking, deciding, contemplating, etc. The mind with
the experience of inward contemplation is Prajna. In the above simile,
the mind takes the place of the ocean, the ship is Prajna and the wind
is the play of the senses.

Complete mastery of the senses is the foundation on which one has to
raise the edifice of Stitha Prajnatva. When the functioning of the
Indriyas is turned inwards, they get merged with the Atman within. The
Atman is not affected thereby even as the ocean remains unaffected by
the waters of the numerous rivers flowing into it. A person whose mind
is so evolved is a Jnani. He is able to distinguish the real from the
unreal and achieve the bliss that flows from the realisation of the
all-pervasiveness of the Paramatma and the identification of the Atma
with the Paramatma. What the worldly minded person imagines as real will
be unreal for such a Jnani. So, Bhagavan wants Arjuna to do his duty,
not with the object of gaining a Kingdom, but the object of acquiring
the mental discipline which will enable him to attain the state of
inaction, namely, Brahmanirvaana or the merger of the Atma with
Paramatma.

July 7, 1958. 

