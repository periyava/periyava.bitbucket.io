Title:Path Of Self-Control

 
 

### PATH OF SELF-CONTROL

Having told Arjuna that a Sthita Prajna is one with an unruffled mind,
one who has completely overcome all desires and passions, Sri Bhagavan
says to Arjuna that such a person withdraws his mind from external
thoughts in order to contemplate the Atman within, which is Full and
All-pervasive, in the same manner as a tortoise withdraws its limbs
within its shell at the slightest sign of danger. We desire a thing
because of a feeling of incompleteness without it. But when we realise
that what we ordinarily understand as "We" is nothing but the Absolute
Bliss, the illusive pleasures after which the senses go lose their
charm. Fire can never be quenched by giving it more fuel; the more we
feed it, the more it burns. Similarly, our desires only increase by
enjoyment.

At the same time, Bhagavan points out to Arjuna, and through Arjuna to
all of us, that the path of self control is not strewn with roses. It
comes only out of constant practice. Failure should not deter us; we
should try, try and try and again till we succeed. Fasting and other
disciplines aid us to acquire self control.

A fasting man or a man suffering from fever is not attracted by sweet
music of delicious food. His mind is not tempted. This does not mean
that these desires have completely disappeared from his mind. When the
fast is broken or when the fever subsides, the mind is again tempted by
these attractions. This shows that the desires remain latent like embers
under ashes. It follows that by merely fasting or observing other forms
of penance, one cannot master the sense-organs and control the mind. It
may appear as if these had been mastered; but at the first trial of
strength, the mind will succumb to temptations. Therefore it is that
Bhagavan has emphasised more than once in the Gita that any one desiring
to acquire true knowledge and understand the Svaroopa of the Paramatma
must surrender himself to Him. Fasting and other forms of discipline
create a proper atmosphere for self-control; but complete self-control
can be acquired only through grace of God. Real control over the mind
comes with the realisation that everything including oneself is
Vasudeva-- Vaasudevassarvamiti. With the dawning of that realisation a
person's senses cease to be attracted by external objects, his mind does
not run away with his sense-organs, and he maintains his mental
equipoise even under the most trying circumstances.

While desire fulfilled leads to further desire, desire frustrated turns
into anger, like the rebound of a ball thrown at a wall. A person in the
grip of desire or anger loses his reasoning power and consequently all
his actions will be in the wrong direction. When desires become
subordinate to the mind, the mind begins to dwell upon the Atman
undisturbed and a person steeped in the contemplation of the Atman
realises the Supreme.

July 6, 1958. 

