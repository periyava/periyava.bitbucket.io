Title:Detachment

 
 

### DETACHMENT

Each one of us is fond of certain things in life, and the liking
develops into raaga, attachment or affection. When the things or persons
we like part from them at the end of life's journey, we are afflicted
with grief. Death forcibly separates us from the objects of our
attachment, resulting only in grief. Death forcibly separates us from
the objects of our attachment, resulting only in grief all round. When
we forcibly pluck an unripe mango, there is weeping(flow of a white
juice) both from the stem to which the mango had all along been
attached, and also from the mango itself. When the same mango is ripe,
it gets automatically separated from the stem and no weeping occurs.
Similarly we must develop the capacity to leave this world without
regret when death knocks at our doors.

How this is to be achieved is the problem of life. I shall illustrate
the answer to this question with a story. Once upon a time a wealthy
person was living in the French territory of India. For some reason, he
apprehended danger both to his person and to his wealth, were he to
continue to live in the French territory. Only a hill separated the
French territory from the British territory. If he could manage to
transport his wealth, which was in the shape of heavy gold and silver
coins, to the top of the hill, safety and security awaited him. But he
found that the task of transporting all the silver and gold coins was an
impossible one, in the circumstances in which he was placed. He was
faced with the situation of leaving behind his immense wealth and
escaping only with his life. At that critical moment, a person with
British currency notes accosted him and offered to exchange those noted
for the gold and silver coins. The wealthy man converted his entire
stock of coins into portable currency notes and crossed over to safety.
Similarly, we should be able to convert all our worldly achievements and
resources into the currency Dharma, so that we can carry with us this
Dharma, when the call comes to quit this world.

Dharma is acquired through mind, speech and deeds. As both Paapa(sin)
and Punya(merit) accompany us after death, we must take care to acquire
only Punya. If we nourish in our mind passions like Kaama(desire) and
Krodha(anger), we will be acquiring only more and more sins. If we use
our speech or power of expression to kindle either Kaama(passion) and
Dvesha(hatred), we will be doing harm to others and thereby hindering
our own emancipation. The gift of speech should be employed only for
doing good to others and repeating the Lord's name. Similarly our
physical strength should be utilised for serving others. Our wealth,
barring a portion we are obliged to leave to our children, should be
utilised for noble and charitable purposes. In this way, we can convert
our material resources and the power of our mind, speech, and body, into
Dharma, the currency note of Isvara, which is legal tender in all the
worlds under His command, and for all times. Dharma alone protects us in
this life and accompanies the soul in its onward march, after it casts
off its covering we call body.

The process of developing detachment from objects of affection- changing
over from raaga to vairaagya - should start when we are still in the
full enjoyment of our senses. When a dispute is compromised, not by the
judgement of a court, but by agreement, the parties to the dispute part
as friends. Similarly, we must mentally become ripe, as the mango I
mentioned earlier, and get ourselves detached from our attachments. For
that purpose we require the grace of Isvara. Sri Sankara Bhagavatpada,
in his Sivananda Lahari prayed to Isvara to save him with His grace or
mercy(krpayaa paalaya vibho) without minding his disqualifications. Let
each of us pray to Isvara to bless us with His grace, for that alone
will accompany the soul and be a source of constant strength.

January 19, 1958 

