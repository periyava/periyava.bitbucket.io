Title:Study Of The Puranas

 
 

### Study of the Puranas

The history we learn in schools and colleges tells us mainly about the
rise and fall of kingdoms, wars and invasions, and similar political
topics. The purpose of history is to enable people in the present to
build for the future, profiting from the experience of the past. The
conception of history is in accord with the saying "history repeats
itself". It is wrong to think that there can be history only for
politics. Every subject has a history behind it.

History is called Itihaasa in Sanskrit. In this country associate
Itihaasa with to works, the Ramayana and the Mahabharatha. The embody
the history of religion, culture, dharma, and the their traditions. The
term Itihaasa is derived by the combination of keywords, iti, he, and
aasa - iti (in this manner), he (they say), aasa (it happened). Aitihyam
means tradition, and it is derived from Itihaasam. Aitihyam has become
Aiteekam in Tamil.

Besides the two Itihaasas, the Ramayana and the Mahabharata, there are
18 Puraanas, which also expound our religion, custom, culture and
traditions. They are very old works as the name Puraana it self
signifies. There are also a large number of works giving that local
traditions of a number of places. They are called Sthala Puraanas. In
the olden days, palm leaf manuscripts of Itihaasas, Puraanas, and Sthala
Puraanas were treasured by householders. If any volume showed signs of
decay , it was copied down on new leaves, and the old manuscript
consigned to the waters of the Kaveri on the 18th day of the month of
Adi (2nd of August). That is how all these ancient works came to be
preserved so long. But owing to the indifference of people in subsequent
periods, the manuscripts were not recopied, and consequently, a bulk of
them got decayed and were lost to us. What could be salvaged are
preserved in the Oriental Manuscripts Library, the Saraswati Mahal
Library(Thanjavur),an dthe Adyar Library. The Theosophical Society has
rendered an invaluable service by collecting and preserving quite a good
number of these vaulable manuscripts. But unfortunately many of the
Sthala Puraanas have been permanently lost to us.

It seems to me that these Sthala Puraanas contain more ethical and moral
lessons and historical facts than even the Puraanas themselves. If we
carefully examine the Puraanas we will be able to find one Puraana
supplemented another. A diligent student, by a co-ordinated study, can
bring to light many truths. The tendency of English-educated persons is
to regard the Puraanic stories as mere fiction. That is not a correct
approach to these valuable works. Have not recent discoveries of fossils
established the existence, at one time, of huge monsters and men of
immense proportions? Do not freaks of nature occur even now? Why then
should we brush aside the Puraanic stories as unbelievable? While
benefiting from the ethical and moreal lessons which these stories
convey, let us also keep an open mind regarding the characters potrayed
in these stories.

In some Puraanas and Sthala maahatyams, we find a reference that Sri
Rama installed a Linga, in order to wash of the sin of Bramha Hathi
which came to be attached to him, as result of killing Ravana, a
Bramhin. Though by killing Ravana, Sri Rama performed a righteous act of
protecting innocent and good men from the tyranny of a bad man, and
though as an incornation of God no sin can ever attach to him, yet as a
model human person, he did this act of expiation as a sin. According to
Sthala Puraanas, Sri Rama is stated to have installed the Linga of
Iswara at Rameswaram, Vedaranyam, and at Pattesvaram, near Kumbakonam,
to expiate respectively the doshas (wrongs) of Bramha-Hathi, Veera-Hathi
and Cchaya-Hathi, resulting from the killing of Ravana. There is
inter-relation between the Sthala Puraanas of these three places and the
Ramayana. One version of the Kaveri Puraana attaches sanctity to the
Amma Mantapam on the banks of the Kaveri at Srirangam, and the center
figure in the story is King Dharma Varma of Nichulapuri(Uraiyoor).
According to another version of the same Puraana, sanctity is attached
to Mayuram and the principal characters in that version are Natha Sarma
and his wife, Anavadyai. It is noteworthy that the bathing ghat or
"lagadam" (a curruption of Thula ghattam), on the banks of Kaveri at
Mayuram and those at six or seven other places are architecturally
similar. In this version of Kaveri Puraana, there is a reference that
Natha Sarma and his wife visited other places of piligrimage like
Kedaara and Kasi. There is bathing ghat none as Kedaara Ghatta at
Banaras, and Sthala Puraana of the place also mentions about the visit
of the Natha Sarma couple to the ghatta. I am mentioning all these facts
to show that one Puraana supplements another and that diligent research
in to these Puraanas and Sthala Puraanas will yield valuable historical
facts.

If our religion survived many vicissitudes in the past, it is because of
our temples and the festivals associated with them. The spiritual,
moral, and ethical principles expounded by the Vedaas have survived and
spread through the Puraanaas. They teach the basic truths in a manner
which appeals to the heart. Let us not, therefore, be indifferent to
these great works of religious literature, but treasure them, study
them, conduct researches in them, and there by benifit ourselves and the
world.

February 4, 1958. 

