
**DIVINE MOTHER - THE BESTOWER OF PROSPERITY**

There is a reference in Mooka Panchasati that Goddess Kamakshi caused a
shower of gold in Tundeeram (Tondaimandalam) to relieve the sufferings
of the people. This allusion is substantiated by the existence of a
place which goes by the name of Pon Vilainda Kalathur in the Chingleput
District. There is also a tradition that Sri Vedanta Desika, the renowed
Sarvatantra Swatantracharya, who established the Vaishnava Sampradaya,
prayed to the Goddess of Wealth, Sri Mahalakshmi, by composing and
reciting Sri Stuti and obtained gold for giving it to a Brahmachari.
This Brahmachari is said to have been sent by some people envious of Sri
Vedanta Desika and this youth begged for gold so that he could get
himself married. This incident also seems to have taken place somewhere
in the region known as Tondaimandalam.

Sri Kanakadharastavam of Sri Sankara Bhagavatpada is a widely known
composition. The circumstances which led to thecomposition and
recitation of this prayer is interesting. After his Upanayana, performed
at a very early age, Sri Sankara rigorously followed the injunctions of
the brahmacharya life and lived on the food obtained by begging or
biksha. In the course of his daily biksha rounds, he stood at the
threshold of a poor Brahmin, one day, and asked for alms, by uttering
the prescribed formula, Bhavati bikshaam dehi. The master of the house,
who himself lived on the charity of his neighbours, was away and the
lady of the house, who possessed a magnanimous heart, wanted to give
something to this child with a divine countenance. Her search resulted
in unearthing only a small amalaka fruit. This she deposited in the
begging bowl devoutly, her heart melting at the thought that she had
nothing better to offer. Sri Sankara divined the situation and realised
that the small gift came from a heart as expansive as the sky itself. He
composed and sang the 18 verses, which go by the name of Sri
Kanakadharastavam. Sri Mahalakshmi responded to the prayer and showered
golden fruits inside the house of the poor Brahmin couple and banished
their poverty. Incidentally this was also the first composition of Sri
Sankara.

There is internal evidence in the stotra itself to substantiate this
story. The verse providing this evidence is :

> *** “Dadyaat dayaanupavano dravina-ambudhaaraam
> Asmin-akinchana vihanga sisau vishanne
> Dushkarma gharmam-apaneeya chiraaya dooram
> Naaraayana-pranayinee nayanaambuvaahah”.***

In this verse, Sri Sankara prays that impelled by the wind of kindness
(dayaanupavano) of Sri Mahalakshmi who is the beloved of Sri Narayana
(Naaraayana pranayini) the cloud of her glance (nayana-ambuvaahah)
should shower (daadyaar) the rain of wealth (dravina-ambudhaaram)
driving away to a distance (apaneeya chiraaya dooram), the scorching
heat (gharmam) of the past sins (dushkarma) of this suffering (vishanne)
fledgling (vihangasisu). The bird referred to here is the mythical
chaataka, which can quench its thirst only when rain falls. The utter
helplessness of the poor householder is indicated by a comparison to the
fledgling of chaataka. To meet the possible objection that the
householder’s present plight is the consequence of his past wrong deeds
(dushkarma), Sri Sankara says that this prayer, on his behalf, should be
sufficient to absolve him of all his past sins.

While the other verses in this stotra are all in praise of the Divine
Mother, this particuolar verse alone contains the request for wealth. In
the same way as showers relieve the parched condition of the scorching
summer heat, he prays that the cooling grace of the Mother should
relieve the sufferings of the poor householder.

> ***“Ishtaavisishtamatayopi yayaa dayaardra-
> Drshtyaa trivishtapapadam sulabham labhante;
> Vrshtih prahrshta kamalodaradeeptirishtaam
> Pushtim krsheeshta mama pushkaravishtaraayaah
> Geerdevateti garudadhwaja-sundareeti
> Sakambhareeti sasisekhara-vallabheti;
> Srshti-sthiti-pralaya kelishu samsthitaayai
> Tasyai namasstribhuvanaikagurostarunyai”.***

In the first of the above two verses, also occurring in Sri
Kanakadhaaraastavam, Sri Sankara prays for the bestowal of desired
prosperity (Ishtaam pushtim), wealth, crop, family, etc. Here Sri
Sankara indicates that even those who perform the prescribed rites to
qualify them for a place in heaven, can attain that status only when the
benevolent glance of the Divine Mother, seated on the lotus, falls on
them. The second verse indicates the true nature of the Mother. It also
teaches that the Divine Mother, known by different names as Saraswati,
Lakshmi, Saakambhari & Parvati, are but manifestations of the same
Supreme Divinity, who is none else than that Creative Energy, who forms
part of the static Paramatma (Tribhuvanaika Guru), both together
constituting the Father and Mother of the universe, and to whom
creation, preservation, and destruction are mere sport (keli).

If we too recite this Sri Kanakadhaaraastavam with devotion, we shall be
relieved of poverty, sufferings and afflictions and sins (daaridrya,
taapa and paapa).


