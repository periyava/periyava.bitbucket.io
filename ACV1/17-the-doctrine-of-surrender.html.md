Title:The Doctrine of Surrender

### The Doctrine of Surrender

> Dhanuh paushpam maurvee madhukaramayee pancha viskikhaa
> Vasantah saamanto Malaya-marud-aayaodhana rathah
> Tatah-api-ekas sarvam himagiri-sute kaam-api krpaam
> Apaangaat-te labdhvaa jagad-idam-anango vijayate


What the grace of the Divine Mother can achieve is illustrated by Sri
Sankara Bhagavathpada in this verse occurring in Soundarya Lahari.
Ananga, Cupid, is able to conquer this world, though he is equipped only
with a bow of sugarcane, whose string is composed of a row of bees, with
five arrows of flowers, with only *Vasanta* or Spring as his lieutenant,
and with the Malaya-breeze as his chariot. Thus ill-equipped, from the
standard of a warrior, he is able to achieve the feat of conquering the
world, because he has obtained the grace of Sri Parvati, daughter of the
snow-capped mountain, conveyed through the glance from the corners of
Her eyes.

 

In this Ananga’s conquest of the world, the bow is sweet and brittle and
the arrows are fragrant and soft. The person who wields the weapon is
Ananga, one without any form. Yet, he derives his strength from the
source of all strength, the grace of the Divine Mother.

 

Sri Parvati as *Daakshayani* consigns Herself to the flame of the
*Yaaga* of Her father, Daksha, unable to bear the abuse heaped upon Her
Lord, Siva, by Her own father, and earned the name of *Sati*. From this,
the expression *Sati* came to be used when any woman immolated herself
in the funeral pyre of her husband.

 

The function of a *Prabhu,* Lord is both protection and punishment. Siva
saved the world from disaster by swallowing the poison generated when
the Ocean of Milk was churned. In other words, He took upon Himself the
sins of the world in order to save humanity. It is this function of God
which Christians attribute to Christ, namely, saving the sinners. Lord
Siva punished Cupid (Kaama) when the latter disturbed  His penance; but
restored him to life, though without form, at the entreaty of Rati and
the intervention of *Parvati*. Sri *Parvati* as Sri *Kamakshi* or Sri
*Sivakamasundari*, is depicted as holding the bow and the arrows of
*Kamadeva*, controlling Kaama within Her eyes. Hence Kamakshi. She holds
the sweet sugarcane bow representing the five senses through which the
mind is influenced and is functioning, and won the grace of Lord Siva,
who alone, as the destroyer of *Kaama* and *Kaala*, is capable of saving
us form the cycle of birth and death. If we surrender ourselves at the
feet of the Divine Mother, in the manner in which Sri Adi Sankara has
taught us in *Soundarya Lahari*. She will help us to keep the mind and
the senses under control and purify our heart, so that we may attain
perfection without being afflicted by *kaama* and *lobha* (lust and
desires) and realise the Ultimate Truth and achieve sublime peace and
happiness.

 

**November 8, 1957**
