Title:Chaaturmasyam Of Sanyasins

 
 

### CHAATURMASYAM OF SANYASINS

Every aasrama has its special dharma or duty. It has been enjoined that
a sanyasi should not remain in one place for any length of lime. He has
to be a parivraajaka or wandering mendicant. The idea is that he should
be moving from place to place, coming into contact with his lay
disciples, ministering to their spiritual needs, and guiding them to
regulate their lives according to the sastras. This may be likened to
"mass contact", a term familiar in politics. If a sanyasi remains in one
place for a long time, there is the danger of his contracting
"attachments", or getting involved in local controversies. There is also
the adage, "familiarity breeds contempt", and, perhaps, that is one of
the reasons why a sanyasi is prohibited from staying long at any one.

This constant movement from place to place may prevent a sanyasi from
devoting sufficient time to meditation and other spiritual practices,
and to the acquisition of aatmajnanam leading to the realisation of the
Ultimate Truth. Therefore, he is permitted to remain in one place during
the chaatur maasya period, commencing from the full moon in the month of
Aani. This period also coincides with the rainy season, known as praavrt
season.

There is a reason behind the selection of this praavrt period for
chaatur maasya. The sanyaasa aasrama is essentially one of ahimsa —
causing no harm to any living being. That is why a sanyasi has to travel
on foot. Even if one were to tread unwittingly on an insect while
walking, there is every chance of one not causing its death, because the
feet are so shaped that the insect can easily wriggle out through the
gaps and curves. During the rainy season, numerous insects spring to
life and infest pathways. Any travel, during this period, will
inevitably lead to himsa, causing pain or injury, to these insects. In
fact, while making the sankalpa for chaatur maasya, a sanyasi has to
tell the assembled devotees that the praavrt period is on, that he sees
a host of insect life (praani sankulam) everywhere, and that if it is
not inconvenient for them, he proposes to observe chaaturmaasyam in that
place. The devotees, who feel honoured by the opportunity for this
kainkarya (service), in their turn, request him to remain in their midst
comfortably, and assure him that they will serve him to the best of
their ability.

Making the chaaturmaasya sankalpa, the sanyasi says:


> Praayena pravrishi praani sankulam varlma drsyate
> Atasleshaam ahimsaarttham pakshaavai srutichoditaan
> Stthaasyaamaschaluromaasaan airaivaasati baadhake.

On hearing this the devotees reply :

> Nivasantu sukhenaatra gamishyaamah krtaartthataam
> Yathaa sakti cha susrooshaam karishyaamo vayam mudaa.

It is to enable sanyasins to adhere to the principle of ahimsa that they
are prohibited from cooking their own food. In the process of cooking,
insects that may happen to be in water, firewood, vegetables, etc., will
be destroyed besides the germinating part of the grain. Therefore
sanyasins are enjoined to rest content with what householders give them
as alms. They are also not permitted to pluck green leaves. That will be
himsa to the plant, which has also life. In fact, there is no agni,
(fire), for the sanyaasa aasrama. That is why they do not perform any
homa (sacrifice in fire).

The chaaturmaasya observance is a common feature of Hinduism, Buddhism,
and Jainism. The Ashokan edicts, which are about 2,000 years old, show
that chaaturmaasya was observed for four months, as the term indicated.
It is not clear when the period came to be reduced to two months.
Probably the rule that a maasa (month) is synonymous with paksha
(fortnight)- pakshovai maasah, - came to be applied, and chaaturmaasya
limited to four pakshaas or two months. This year, chaaturmaasya will
last three months, on account of the occurrence of the Adhika Sraavana
month. (The difference in the number of days that go to make a solar
year and a lunar year get periodically adjusted by having an additional
lunar month whenever two new moons happen to fall in a solar month.)

There is a reference to chaaturmaasya in Srimad Bhagavatam also. It is
recorded that when Sage Narada was asked how he became a great jnani, he
replied that in his boyhood a number of sanyasins happened to observe
chaaturmaasya at the place where he lived with his mother, and that
jnana dawned on him, as a result of eating the remnants of the food
partaken by those great men.

A sanyasi takes the resolve to observe chaaturmaasya after performing
Vyasa Pooja. This pooja is as important to sanyasins as Upaakarma is to
those who belong to the other aasramas. As custodians of the Vedas, it
is our duty to preserve them in their pristine purity and effectiveness.
The danda (stick), carried by a brahmachari, is symbolic of his
determination to protect the Vedas at any cost. The object of Upaakarma
is to revitalise the Vedic mantraas, should their efficacy be impaired,
through causes like faulty pronunciation. The Vedas are recited on that
day, after invoking the grace of Sri Veda Vyasa, who perceived, through
his spiritual powers, the Vedas and transmitted them for the benefit of
the world, and invoking the grace of the rishis, who propagated the
various khaandaas of the Vedas. The presence of Sri Veda Vyasa is
invoked in a pot of water and worshipped. The Sama Vedins invoke the
presence of Khaanda Rishis in balls of earth, or in arecanuts and
worship them. Similarly the sanyasins invoke the grace of Sri Veda Vyasa
and other preceptors of aatma jnaana, before commencing their discipline
of meditation, yoga, and aatmavichaara. The aavahana of the preceptors
is done in lime fruits. Householders perform both pooja and homa in
Upaakarma, whereas sanyasins perform only pooja on Vyasa Pooja day, as
they have no right to do homa.

It is not Veda Vyasa alone who is worshipped on Vyasa Pooja day. Six
groups of preceptors (moola purushas) of jnana, each group consisting of
five preceptors are worshipped. The first group is called Krishna
Panchaka and consists of Sri Krishna, Vaasudeva, Pradyumna, Anirudha and
Sankarshana. The five groups, besides the Sri Krishnapanchaka mentioned
above, are: (1) The Vyasa Panchaka, consisting of Sri Vyasa, Sri Paila,
Sri Vaisampayana, Sri Jaimini, and Sri Sumantu; (2) The Bhagavatpada
Panchaka, consisting of Sri Sankara Bhagavatpada, Sri Padmapaadaacharya,
Sri Sureswaraachaarya, Sri Hastaamalakaacharya, and Sri Totakaachaarya;
(3) The Sanaka Panchaka, consisting of Sri Sanaka, Sri Sanandana, Sri
Sanaatana, Sri Sanat Kumara, and Sri Sanatsujaata; (4) The Dravida
Panchaka, consisting of Sri Dravidaacharya, Sri Gaudaapadaacharya, Sri
Govinda Bhagavatpaadaacharya, Sri Sankshepakaacharya, and Sri
Vivaranaacharya; and (5) The Guru Panchaka, consisting of the Guru, the
Parama Guru, the Parameshti Guru, and the Paraapara Guru of the
sanyasins, and other promulgators of the sampradaya (anye brahmavidyaa
sampradaaya kartaarah guravah). Worship is also offered to Sri Suka, Sri
Narada, Sri Durga, Sri Ganapati, the Kshetrapaalaas, Sri Saraswati, and
the ten guardians of the directions, beginning with Indra. Finally pooja
is offered 10 Suddha Chaitanya, whose aavaahana is made in the
Saaligramah and omnibus worship (samashti pooja) is performed at the
end.

The seniority of a sanyasi is determined, not by his age, but by the
number of Vyasa Poojas he has performed. It may happen that a young
sanyasi has performed more Vyasa Poojas than an aged one. In that case,
the aged sanyasi will have to do obeisance to the young one. This
practice is similar to the practice among householders of a person
prostrating before a lady younger than himself should the husband of
that lady be senior to him.

What is known as Vyasa Poornima in the South is known as Guru Poornima
in the North. On that day, every person makes it a point to make
offerings to all those who occupy the position of teacher to him. This
custom is followed in the R.S.S. organisation. Its members pay no
subscription, but make a cash offering on the Guru Poornima day. For the
Smartha sanyasins, the chaaturmaasya begins with Vyasa Pooja and ends
with Viswaroopa Yatra. The Vaishnavas commence chaaturmaasya with
sankalpam and end with utthaanam.

Sri Krishna is called the lord of cows. Sri Krishna being the central
figure in Vyasa Pooja, 1 desire to give you the message of
gosamrakshanam (cow protection). It is an irony that in the land where
the cow is worshipped, cows are found in an emaciated condition. In
lands where cow-slaughter is not regarded as a sin, the cattle yield
more milk per head, and are better looked after. In India, the peasant
is perpetually indebted and his cattle are mere skin and bones. In the
past, this neglect was trotted out as an excuse for not bringing in
legislation to prevent cow slaughter. The Government, as well as the
public, owe a duty to attend to the cattle the wealth of a country. In
the ancient days the village had a common pasture called meichal tarai.
These pasture lands have got assigned to private individuals. The
Government should take steps to acquire these pasture lands for the
benefit of the cattle. They should also include the maintenance of
common village tanks, known as mantaikarai kulam under their minor
irrigation works. So far as the public is concerned, each house should
keep a vessel or a bucket for collecting the water with which rice is
washed for cooking, as well as kanji and kitchen garbage, like discarded
portions of vegetables, skins of fruits, plantain leaves, etc.
Arrangements should be made to feed cows with this collection. If every
house-holder takes care of one cow in this manner, the cattle wealth of
the country will improve in no time.


