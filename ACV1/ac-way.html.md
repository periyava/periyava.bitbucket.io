Title:The Way Of Knowledge

 
 

### THE WAY OF KNOWLEDGE

In the third chapter of the Gita, we have seen how Sri Krishna stressed
the Supreme importance of Karma Yoga and impressed on Arjuna the
necessity to do his natural duty, even if he had attained the Jnana that
qualified him to rise above all routines ceremonials. Bhagavan told
Arjuna that such unattached performance of Karma was necessary in the
larger interest of the welfare of the world. The performance of the
Karmas prescribed in the Vedas, and the duties pertaining to each
person's station in life, is as sure means to get rid of the impurity of
the heart and to keep the mind under control, so that the individual
Atma may realise its real nature, namely, that it is an infinitesimal
fraction of the ocean of Paramatma, and, in that realisation, become
merged in the Eternal and Supreme Bliss. Bhagavan also warns Arjuna that
the two enemies of our spiritual progress are Kaama and Krodha, desire
and anger, and tells him that he must do his duty with all the intensity
he is capable of, free from even the faintest taint of Kaama and Krodha.

It is against this background that Sri Krishna delivers His message
contained in the fourth chapter. In a past age, Bhagavan says, He had
given this message to the world through Vivasvaan, the Sun, and He was
now again giving the secret of this Yoga to Arjuna, because he has
surrendered himself to Him as a devotee (Bhakta) and also as a friend
(Sakha). This created for Arjuna the natural difficulty of associating
Sri Krishna with Bhagavan, who first gave this message to the Sun. This
doubt raised by Arjuna was cleared by Bhagavan by giving him a glimpse
of His real nature, through the memorable verses in the Fourth chapter.
He also lets Arjuna into the secret of His Avatars, as stated in the
often-quoted verse:

> yadaa yadaahi dharmasya
> glaanirbhavati bhaarata
> abhyutthaanam adharmasya
> tadaatmaanam srijaamyaham.

The main point to be noted is that He is born from time to time to save
humanity from perishing, by arresting its course along the wrong path
and guiding its feet again along the right path. When we say He is born,
we have to bear in mind one important difference. Bhagavan Himself
proclaims that He has neither beginning nor end (birth or death), and
the He is the Supreme Isvara of the Universe. So, He is not born in the
ordinary sense, but born out of his own Maaya(Atma Maaya). An actor, who
is a distinct individual in private life, appears on the stage in one
role today and another role tomorrow. The real personality of the actor
is hidden behind the make-up on the stage. On the stage he is a
different person each day. God is eternal and changeless. But He appears
to assume different forms on account of the drapings, which is maaya,
that cover His real personality. The static Isvara or Purusha appears to
function in infinite ways in this Universe, because of the impact of
Maaya or Prakriti, which in its turn drives its energy from Him, the
reservoir of all energies. He is conscious of His avatars, because He
has never ceased to exist; but Arjuna (by implication, the entire
humanity) is not conscious of the several births taken by him, because
his awareness is limited to present birth. Though the Atma is but a
spark of the Paramatma, it is wrapped up in ignorance or Ajnaana, on
account of the operation of emotions like raaga(desire), krodha(anger),
and bhaya(fear) and is not, therefore, able to know itself. Man is born
subject to the play of these emotions, while bhagavan, who transcends
all these emotions, while appearing to be born, is in reality birthless.

Bhagavan tells Arjuna that he who is able to pierce through the wheel of
His apparent birth through Jnaana, and see Him as He is, will be able to
transcend birth and death and realise Him. How to achieve this is
explained in this chapter. The emphasis is again on getting rid of the
emotions caused by the promptings of the senses(veeta raaga bhaya
krodha). Those who succeed in this task are able first to contemplate
Him uninterruptedly(manmayaa) then surrender themselves unreservedly to
Him(maamupaasritah) and finally get merged in Him (madbhaava maagataah).

Ordinarily people are inclined to perform Karma or to worship one or the
other manifestations of God for obtaining quick results in the material
sphere. But Isvara has no likes and dislikes and showers His grace on
all, each getting what he is qualified of or deserves to receive. As
stated in the subsequent chapter, He is worship by four distinct types
of persons - aartah(those in trouble), jijnaasuh(those thirsting for
true knowledge), artthaarthi(those who wish to be happy always) and
jnaani(those who are aware of him). Among them, the jnaani alone gets
freed from further births and gets merged in Him.

For every effect there is a cause, ordinary and extraordinary. Ordinary
cause can be illustrated by saying that yarn is the cause of cloth.
Isvara is the extraordinary cause for the functioning of the Universe.
The path for each is chalked out by Him. Even hatred for God becomes the
cause of salvation, as in the case of Hiranyakasipu and Kamsa, for, each
of them, in His intense hatred, began to think constantly of God.

The key for understanding the real nature of God is to realise that
thought the entire Universe functions on account of Him, He is not the
doer. He is akarta and He is unattached, both to the actions and to the
results flowing from those actions. Realising this, if we do our
prescribed task, without attachment or expectation of results, we
gradually become Braahman Himself. That is the path followed by the
great men in the past, and that is the path shown in the Geetha by
Bhagavan Krishna to Arjuna and all of us. Good deed s wipe out the bad
karmaas of the past and by acting in a spirit of dedication, the mind
becomes pure. When devotion is added to disinterested act, jnaana or
Ultimate Realisation results.

July 10, 1958. 

