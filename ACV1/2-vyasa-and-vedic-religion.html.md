Title:Vyasa and Vedic Religion

### Vyasa and Vedic Religion

 

 

Sage Vyasa is known as Veda Vyasa, as he classified and compiled
together, the vast body of Vedas or *mantras* then existing. He
classified the Vedas in four, namely *Rig, Yajur, Sama and Atharvana*
and taught them respectively to four great Rishis – *Sumantu,
Vaisampayane, Jaimini and Paila*. Mantras are present around us as sound
waves, and they are without beginning or end. As a radio set picks up a
broadcast sound, so also these great *Rishis,* by their yogic power,
were able to comprehend and master these sound waves vibrating around
them. One meaning of the word *Rishi* is that person who has seen the
*mantras*. (Rishayo mantra-drashtaarah) Yogic power endowed them with
spiritual eyes with which they saw and registered in their minds the
forms of these mantras, even as Arjuna was able to see before him the
Viswaroopa of the Lord The Vedas have thus come down to us in their
original form by the process of oral transmission from Guru to be
Sishya. The Vedas have to be learnt by competent persons in an attitude
of devotion, and with due observances of austerities and preserved for
posterity.

 

Sage Vyasa also composed the 18 *Puranas*, which contain the purport of
the Vedas and asked *Soota*, a sage revered for his knowledge and
devotion, to teach them to the world. The next great service that Sri
Vyasa did was to write a compendium of the truth of the Vedas in
aphoristic from known as *Brahma Sutras*. The *Brahma Sutras* were
interpreted by the great Acharyas, who came later, in their commentaries
or *Bhashyas*. The commentaries most widely read are those of Sri Adi
Sankara, Sri Ramanuja, and Sri Madhwa. Whatever doctrinal differences
may have arisen in later times, we should not forget that the authority
or the source of these commentaries is the *Brahma Sutras* of Sri Veda
Vyasa. India has evoked the esteem and admiration of other countries for
this remarkable achievement in the realm of spiritual culture and
metaphysical thinking. It is our duty to adore the great Sage Vyasa, who
has made available to us the Vedas and remember with gratitude the great
*Rishis* who preserved them and passed them on to posterity in their
original purity by this process of oral transmission.

 

In addition to the Vedas, we have the body of *Dharma Sutras* which tell
us what we should do and should not do, to qualify ourselves for the
study of the Vedas and which tell us how to practise our religion. They
are also known as the *Smritis* and are associated with the names of the
great Rishis like Parasara, Yaajnavalkya, Manu and others. Compendiums
of these Smritis known as *Dharma-sastra-nibandhanam* have been written
by later authors. In the North the most popular *Nibandhanam* is the one
written by Kasinath Upadhyaya, while in the South, it is that written by
Vaidyanatha Dikshitar. The Vaidyanatha Dikshiteeyam is common to both
Vaishnavites and Saivites. Thus the Vedas and the *Dharma Sastras* are
the foundation of our religion.

 

One important difference between other religions and ours is that while
other religions speak of a direct relation between man and God, our
religion speaks of a meditated relation established through
transcendental deities, each presiding over a particular aspect of
worldly and spiritual life. Sri Krishna says in the Gita that when
*Prajapati* created men. He did so associating them with the obligation
to perform *yajnas* or sacrifices. The *yajnas* are our expression of
gratitude for benefits derived. The gods accepted our offerings through
the sacrificial fire and blessed us in return with all the good things
of the world. As an after dinner toast honours even an absent person in
whose name it is proposed, the offerings made in the fire in a spirit of
sacrifice saying “*na mama”* (not mine), bring gratification to the
gods to whom they are intended. The *Vedic* rituals in a *yajna* are the
process by which whatever is offered with a sense of renunciation is
transmitted to the Supreme Being through the proper channel, just as
taxes are paid by us to the Central Government, not directly, but
through the persons or agencies authorised to collect them. According to
our religion, direct relation between man and God can be established
only when on is nearest to God. Such persons are *Brahmajnanis* and
*Sanyasis* and they do not have to do any ritual prescribed in
*Smritis.* All others have to perform the rituals or *karmas* prescribed
for them.

 

We must perform the *Deva-karmas* and the *Pitru-karmas* enjoined upon
us and, understanding the rationale behind such observances of *karmas*,
preserve the Vedas and the *Dharma Sastras,* and also remember with
reverence and gratitude Sri Veda Vyasa, the *Moola Purusha* of our
religion.

 

 

**October 14, 1957
