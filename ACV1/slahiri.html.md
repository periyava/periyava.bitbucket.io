Title:Soundarya Lahiri

MESSAGE OF SOUNDARYA LAHARI

> ***“Pradeepa jvaalaabhir-divasakara neeraajana vidhih
> Sudhaa sootes-chandraopala jala lavair-arghya rachanaa;
> Swakeeyair-ambhobhih salila nidhi sauuhitya karanam
> Tvadeeyaabhir-vaagbhi stava janani vaachaam stutiriyam”***

Composing these verses in praise of You, O Mother, in words originating
from You, is like worshipping the Sun by waving a light, offering arghya
to the Moon with drops of water dripping from a moon-stone and bathing
the ocean with its own water.

This is the last verse in Soundarya Lahari composed by Sri Adi Sankara.
Through this verse, he brought home to us the truth that all virtues and
skills we claim to posses are derived from the Supreme Mother through
Her grace. The Soundarya Lahari is a composition, the beauty of which
has not so far been surpassed. It is in praise of Ambika, Herself the
embodiment and source of all beauty. The beautiful words in which
Soundarya Lahari is composed are also derived through the grace of
Ambika. That is why Sri Sankara Bhagavatpada has expressed in this verse
that singing the praise of Ambika in the composition, Soundarya Lahari,
in words originating from Her, is very much like worshipping the Sun by
waving a lighted camphor before him, or offering the Moon drops of
nectar emitted by the Chandrakaanta stone under the influence of the
Moon, or bathing the ocean with its own waters. The lesson to be drawn
is that whenever any honour is done to a person, the recipient must
remember the divine source from which he derived the qualifications to
receive that honour, and feel humble and not elated with a feeling of
self-importance.

Sri Sankara Bhagavatpada, within the short span of his life, made
tremendous achievements. The world of intellect was at his feet. The
influence of all other creeds vanished into thin air. His fame travelled
far beyond the shores of India. A stone inscription recovered from a
temple in ruins in the jungles of Cambodia mentioned that the temple was
built by a King whose guru claimed to be a descendent of a pupil of
Bhagavan Sankara. French archaeologists have recovered from Cambodia 700
to 800 Sankrit incriptions in stone. All the inscriptions are in
beautiful Sanskrit.

> ***“ Yenaadheetani saastraani bhagavat-sankaraahvayaat;
> Nissesha soori moordhaali maala leedhaanghripankajaat”.***

This verse emphasises the greatness of Sri Sankara. It says that all the
great seekers of truth (soori) in the country, without exception,
acknowledged the greatness of Sri Sankara by bowing their heads at his
lotus feet. Such a great soul felt humble after composing Soundarya
Lahari and dedicated it to the Supreme Mother. In that way, he taught
the world and us the lesson of humility and the need for eschewing from
one’s nature egoism or arrogance, realising that all merits are derived
from the divine source.

October 11, 1957


